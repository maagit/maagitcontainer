<?php
namespace Maagit\Maagitcontainer\Hooks\Datahandler;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Hooks
	class:				DatamapPreProcessFieldArrayHook

	description:		Datamap pre process field array

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class DatamapPreProcessFieldArrayHook
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $tcaRegistry;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory
	 */
	protected $containerFactory;

	/**
	 * @var \Maagit\Maagitcontainer\Hooks\Datahandler\Database
	 */
	protected $database;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Service\ContainerService
	 */
	protected $containerService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function __construct(\Maagit\Maagitcontainer\Domain\Factory\ContainerFactory $containerFactory, \Maagit\Maagitcontainer\Hooks\Datahandler\Database $database, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \Maagit\Maagitcontainer\Domain\Service\ContainerService $containerService)
	{
		$this->containerFactory = $containerFactory;
		$this->database = $database;
		$this->tcaRegistry = $tcaRegistry;
		$this->containerService = $containerService;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function processDatamap_preProcessFieldArray(array &$incomingFieldArray, string $table, $id, \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler): void
	{
		if ($table !== 'tt_content')
		{
			return;
		}
		if (\TYPO3\CMS\Core\Utility\MathUtility::canBeInterpretedAsInteger($id))
		{
			return;
		}
		if (!isset($incomingFieldArray['pid']) || (int)$incomingFieldArray['pid'] >= 0)
		{
			return;
		}
		$incomingFieldArray = $this->newElementAfterContainer($incomingFieldArray);
		$incomingFieldArray = $this->copyToLanguageElementInContainer($incomingFieldArray);
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function newElementAfterContainer(array $incomingFieldArray): array
	{
		if (isset($incomingFieldArray['tx_maagitcontainer_parent']) && (int)$incomingFieldArray['tx_maagitcontainer_parent'] > 0)
		{
			return $incomingFieldArray;
		}
		$record = $this->database->fetchOneRecord(-(int)$incomingFieldArray['pid']);
		if ($record === null || $record['tx_maagitcontainer_parent'] > 0)
		{
			// new elements in container have already correct target
			return $incomingFieldArray;
		}
		if (!$this->tcaRegistry->isContainerElement($record['CType']))
		{
			return $incomingFieldArray;
		}
		try
		{
			$container = $this->containerFactory->buildContainer($record['uid']);
			// @extensionScannerIgnoreLine
			if ($container->getLanguage() === 0 || !$container->isConnectedMode())
			{
				$incomingFieldArray['pid'] = $this->containerService->getAfterContainerElementTarget($container);
			}
		}
		catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
		{
			
		}
		return $incomingFieldArray;
	}

	protected function copyToLanguageElementInContainer(array $incomingFieldArray): array
	{
		if (!isset($incomingFieldArray['tx_maagitcontainer_parent']) || (int)$incomingFieldArray['tx_maagitcontainer_parent'] === 0)
		{
			return $incomingFieldArray;
		}
		if (!isset($incomingFieldArray['l10n_source']) || (int)$incomingFieldArray['l10n_source'] === 0)
		{
			return $incomingFieldArray;
		}
		if (!isset($incomingFieldArray['l18n_parent']) || (int)$incomingFieldArray['l18n_parent'] > 0)
		{
			return $incomingFieldArray;
		}
		if (!isset($incomingFieldArray['sys_language_uid']) || (int)$incomingFieldArray['sys_language_uid'] === 0)
		{
			return $incomingFieldArray;
		}
		$record = $this->database->fetchOneRecord(-$incomingFieldArray['pid']);
		$translatedContainerRecord = $this->database->fetchOneTranslatedRecordByl10nSource((int)$incomingFieldArray['tx_maagitcontainer_parent'], (int)$incomingFieldArray['sys_language_uid']);
		if ($record === null || $translatedContainerRecord === null)
		{
			return $incomingFieldArray;
		}
		try
		{
			$incomingFieldArray['tx_maagitcontainer_parent'] = $translatedContainerRecord['uid'];
			$container = $this->containerFactory->buildContainer($translatedContainerRecord['uid']);
			if ((int)$record['sys_language_uid'] === 0 || empty($container->getChildrenByColPos((int)$incomingFieldArray['colPos'])))
			{
				$target = $this->containerService->getNewContentElementAtTopTargetInColumn($container, (int)$incomingFieldArray['colPos']);
				$incomingFieldArray['pid'] = $target;
			}
		}
		catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
		{
			// not a container
		}
		return $incomingFieldArray;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}