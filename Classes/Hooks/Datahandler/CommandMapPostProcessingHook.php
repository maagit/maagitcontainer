<?php
namespace Maagit\Maagitcontainer\Hooks\Datahandler;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Hooks
	class:				CommandMapPostProcessingHook

	description:		Command map post processing

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CommandMapPostProcessingHook
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory
	 */
	protected $containerFactory;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Domain\Factory\ContainerFactory $containerFactory)
	{
		$this->containerFactory = $containerFactory;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function processCmdmap_postProcess(string $command, string $table, int $id, $value, \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler, $pasteUpdate, $pasteDatamap): void
	{
		if ($table === 'tt_content' && $command === 'copy' && !empty($pasteDatamap['tt_content']))
		{
			$this->copyOrMoveChildren($id, (int)$value, (int)array_key_first($pasteDatamap['tt_content']), 'copy', $dataHandler);
		}
		elseif ($table === 'tt_content' && $command === 'move')
		{
			$this->copyOrMoveChildren($id, (int)$value, $id, 'move', $dataHandler);
		}
		elseif ($table === 'tt_content' && ($command === 'localize' || $command === 'copyToLanguage'))
		{
			$this->localizeOrCopyToLanguage($id, (int)$value, $command, $dataHandler);
		}
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function localizeOrCopyToLanguage(int $uid, int $language, string $command, \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler): void
	{
		try
		{
			$container = $this->containerFactory->buildContainer($uid);
			$children = $container->getChildRecords();
			$children = array_reverse($children);
			$cmd = ['tt_content' => []];
			foreach ($children as $colPos => $record)
			{
				$cmd['tt_content'][$record['uid']] = [$command => $language];
			}
			if (count($cmd['tt_content']) > 0)
			{
				$localDataHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
				// @extensionScannerIgnoreLine
				$localDataHandler->start([], $cmd, $dataHandler->BE_USER);
				$localDataHandler->process_cmdmap();
			}
		}
		catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
		{
			// nothing todo
		}
	}

	protected function copyOrMoveChildren(int $origUid, int $newId, int $containerId, string $command, \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler): void
	{
		try
		{
			// when moving or copy a container into other language the other language is returned
			$container = $this->containerFactory->buildContainer($origUid);
			(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Hooks\Datahandler\DatahandlerProcess::class))->startContainerProcess($origUid);
			$children = [];
			$colPosVals = $container->getChildrenColPos();
			foreach ($colPosVals as $colPos)
			{
				$childrenByColPos = $container->getChildrenByColPos($colPos);
				$childrenByColPos = array_reverse($childrenByColPos);
				foreach ($childrenByColPos as $child)
				{
					$children[] = $child;
				}
			}
			if ($newId < 0)
			{
				$previousRecord = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('tt_content', abs($newId), 'pid');
				$target = (int)$previousRecord['pid'];
			}
			else
			{
				$target = $newId;
			}
			foreach ($children as $record)
			{
				$cmd = [
					'tt_content' => [
						$record['uid'] => [
							$command => [
								'action' => 'paste',
								'target' => $target,
								'update' => [
									'tx_maagitcontainer_parent' => $containerId,
									'colPos' => $record['colPos'],
								],
							],
						],
					],
				];
				$origCmdMap = $dataHandler->cmdmap;
				// @extensionScannerIgnoreLine
				if (isset($origCmdMap['tt_content'][$origUid][$command]['update']['sys_language_uid']))
				{
					// @extensionScannerIgnoreLine
					$cmd['tt_content'][$record['uid']][$command]['update']['sys_language_uid'] = $origCmdMap['tt_content'][$origUid][$command]['update']['sys_language_uid'];
				}
				$localDataHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
				// @extensionScannerIgnoreLine
				$localDataHandler->start([], $cmd, $dataHandler->BE_USER);
				$localDataHandler->process_cmdmap();
			}
			(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Hooks\Datahandler\DatahandlerProcess::class))->endContainerProcess($origUid);
		}
		catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
		{
			// nothing todo
		}
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}