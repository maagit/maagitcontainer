<?php
namespace Maagit\Maagitcontainer\Hooks\Datahandler;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Hooks
	class:				CommandMapBeforeSartHook

	description:		Command map before start

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CommandMapBeforeStartHook
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $tcaRegistry;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory
	 */
	protected $containerFactory;

	/**
	 * @var \Maagit\Maagitcontainer\Hooks\Datahandler\Database
	 */
	protected $database;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Service\ContainerService
	 */
	protected $containerService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Domain\Factory\ContainerFactory $containerFactory, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \Maagit\Maagitcontainer\Hooks\Datahandler\Database $database, \Maagit\Maagitcontainer\Domain\Service\ContainerService $containerService)
	{
		$this->containerFactory = $containerFactory;
		$this->tcaRegistry = $tcaRegistry;
		$this->database = $database;
       	$this->containerService = $containerService;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function processCmdmap_beforeStart(\TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler): void
	{
		$this->unsetInconsistentLocalizeCommands($dataHandler);
		$dataHandler->cmdmap = $this->rewriteSimpleCommandMap($dataHandler->cmdmap);
		$dataHandler->cmdmap = $this->extractContainerIdFromColPosOnUpdate($dataHandler->cmdmap);
		// previously page id is used for copy/moving element at top of a container colum
		// but this leeds to wrong sorting in page context (e.g. List-Module)
		$dataHandler->cmdmap = $this->rewriteCommandMapTargetForTopAtContainer($dataHandler->cmdmap);
		$dataHandler->cmdmap = $this->rewriteCommandMapTargetForAfterContainer($dataHandler->cmdmap);
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function rewriteCommandMapTargetForAfterContainer(array $cmdmap): array
	{
		if (!empty($cmdmap['tt_content']))
		{
			foreach ($cmdmap['tt_content'] as $id => &$cmd)
			{
				foreach ($cmd as $operation => $value)
				{
					if (in_array($operation, ['copy', 'move'], true) === false)
					{
						continue;
					}
					// @extensionScannerIgnoreLine
					if ((!isset($value['update']['tx_maagitcontainer_parent']) || (int)$value['update']['tx_maagitcontainer_parent'] === 0) && ((is_array($value) && $value['target'] < 0) || (int)$value < 0))
					{
						if (is_array($value))
						{
							$target = -(int)$value['target'];
						}
						else
						{
							// simple command
							$target = -(int)$value;
						}
						$record = $this->database->fetchOneRecord($target);
						if ($record === null || $record['tx_maagitcontainer_parent'] > 0)
						{
							// elements in container have already correct target
							continue;
						}
						if (!$this->tcaRegistry->isContainerElement($record['CType']))
						{
							continue;
						}
						try
						{
							$container = $this->containerFactory->buildContainer($record['uid']);
							$target = $this->containerService->getAfterContainerElementTarget($container);
							if (is_array($value))
							{
								$cmd[$operation]['target'] = $target;
							}
							else
							{
								// simple command
								$cmd[$operation] = $target;
							}
						}
						catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
						{
							continue;
						}
					}
				}
			}
		}
		return $cmdmap;
	}

	protected function rewriteCommandMapTargetForTopAtContainer(array $cmdmap): array
    {
		if (!empty($cmdmap['tt_content']))
		{
            foreach ($cmdmap['tt_content'] as $id => &$cmd)
			{
                foreach ($cmd as $operation => $value)
				{
                    if (in_array($operation, ['copy', 'move'], true) === false)
					{
                        continue;
                    }
					// @extensionScannerIgnoreLine
					if (isset($value['update']) && isset($value['update']['tx_maagitcontainer_parent']) && $value['update']['tx_maagitcontainer_parent'] > 0 && isset($value['update']['colPos']) && $value['update']['colPos'] > 0 && $value['target'] > 0)
					{
						try
						{
							// @extensionScannerIgnoreLine
							$container = $this->containerFactory->buildContainer((int)$value['update']['tx_maagitcontainer_parent']);
							// @extensionScannerIgnoreLine
							$target = $this->containerService->getNewContentElementAtTopTargetInColumn($container, (int)$value['update']['colPos']);
							$cmd[$operation]['target'] = $target;
						}
						catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
						{
							// not a container
						}
					}
				}
			}
		}
		return $cmdmap;
	}
	
	protected function rewriteSimpleCommandMap(array $cmdmap): array
	{
		if (!empty($cmdmap['tt_content']))
		{
			foreach ($cmdmap['tt_content'] as $id => &$cmd)
			{
				if (empty($cmd['copy']) && empty($cmd['move']))
				{
					continue;
				}
				foreach ($cmd as $operation => $value)
				{
					if (in_array($operation, ['copy', 'move'], true) === false)
					{
						continue;
					}
					if (is_array($cmd[$operation]))
					{
						continue;
					}
					if ((int)$cmd[$operation] < 0)
					{
						$target = (int)$cmd[$operation];
						$targetRecordForOperation = $this->database->fetchOneRecord((int)abs($target));
						if ($targetRecordForOperation === null)
						{
							continue;
						}
						if ((int)$targetRecordForOperation['tx_maagitcontainer_parent'] > 0)
						{
							// record will be copied/moved into container
							$cmd = [
								$operation => [
									'action' => 'paste',
									'target' => $target,
									'update' => [
										'colPos' => $targetRecordForOperation['tx_maagitcontainer_parent'].'-'.$targetRecordForOperation['colPos'],
										'sys_language_uid' => $targetRecordForOperation['sys_language_uid'],
									],
								],
							];
						}
						elseif ($this->tcaRegistry->isContainerElement($targetRecordForOperation['CType']))
						{
							// record will be copied/moved after container
							$cmd = [
								$operation => [
									'action' => 'paste',
									'target' => $target,
									'update' => [
										'colPos' => (int)$targetRecordForOperation['colPos'],
										'sys_language_uid' => $targetRecordForOperation['sys_language_uid'],
									],
								],
							];
						}
					}
				}
			}
		}
		return $cmdmap;
	}

	protected function unsetInconsistentLocalizeCommands(\TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler): void
	{
		if (!empty($dataHandler->cmdmap['tt_content']))
		{
			foreach ($dataHandler->cmdmap['tt_content'] as $id => $cmds)
			{
				foreach ($cmds as $cmd => $data)
				{
					if ($cmd === 'localize')
					{
						$record = $this->database->fetchOneRecord((int)$id);
						if ($record !== null && $record['tx_maagitcontainer_parent'] > 0)
						{
							$container = $this->database->fetchOneRecord($record['tx_maagitcontainer_parent']);
							if ($container === null)
							{
								// should not happen
								continue;
							}
							$translatedContainer = $this->database->fetchOneTranslatedRecordByLocalizationParent($container['uid'], (int)$data);
							if ($translatedContainer === null || (int)$translatedContainer['l18n_parent'] === 0)
							{
								$dataHandler->log('tt_content', $id, 1, 0, 1, 'Localization failed: container is in free mode or not translated', 28);
								unset($dataHandler->cmdmap['tt_content'][$id][$cmd]);
								if (!empty($dataHandler->cmdmap['tt_content'][$id]))
								{
									unset($dataHandler->cmdmap['tt_content'][$id]);
								}
							}
						}
					}
				}
			}
		}
	}

	protected function extractContainerIdFromColPosOnUpdate(array $cmdmap): array
	{
		if (!empty($cmdmap['tt_content']))
		{
			foreach ($cmdmap['tt_content'] as $id => &$cmds)
			{
				foreach ($cmds as &$cmd)
				{
					// @extensionScannerIgnoreLine
					if ((!empty($cmd['update'])) && isset($cmd['update']['colPos']))
					{
						// @extensionScannerIgnoreLine
						$cmd['update'] = $this->dataFromContainerIdColPos($cmd['update']);
					}
				}
			}
		}
		return $cmdmap;
	}

	protected function dataFromContainerIdColPos(array $data): array
	{
		$colPos = $data['colPos'];
		if (\TYPO3\CMS\Core\Utility\MathUtility::canBeInterpretedAsInteger($colPos) === false)
		{
			[$containerId, $newColPos] = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode('-', $colPos);
			$data['colPos'] = $newColPos;
			$data['tx_maagitcontainer_parent'] = $containerId;
		}
		elseif (strpos((string)$colPos, (string)\Maagit\Maagitcontainer\Backend\Grid\ContainerGridColumn::CONTAINER_COL_POS_DELIMITER) > 0)
		{
			$pos = strripos((string)$colPos, (string)\Maagit\Maagitcontainer\Backend\Grid\ContainerGridColumn::CONTAINER_COL_POS_DELIMITER);
			$splitted = \TYPO3\CMS\Core\Utility\GeneralUtility::intExplode((string)\Maagit\Maagitcontainer\Backend\Grid\ContainerGridColumn::CONTAINER_COL_POS_DELIMITER, $colPos, true);
			$newColPos = (int)array_pop($splitted);
			$containerId = (int)substr((string)$colPos, 0, $pos);
			$data['colPos'] = $newColPos;
			$data['tx_maagitcontainer_parent'] = $containerId;
		}
		elseif (!isset($data['tx_maagitcontainer_parent']))
		{
			$data['tx_maagitcontainer_parent'] = 0;
			$data['colPos'] = (int)$colPos;
		}
		return $data;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}