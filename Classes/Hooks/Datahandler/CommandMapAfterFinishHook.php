<?php
namespace Maagit\Maagitcontainer\Hooks\Datahandler;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Hooks
	class:				CommandMapAfterFinishHook

	description:		Command map after finish

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class CommandMapAfterFinishHook
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Hooks\Datahandler\Database
	 */
	protected $database;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Hooks\Datahandler\Database $database)
	{
		$this->database = $database;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function processCmdmap_afterFinish(\TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler): void
	{
		$cmdmap = $dataHandler->cmdmap;
		$copyMappingArray_merged = $dataHandler->copyMappingArray_merged;
		foreach ($cmdmap as $table => $incomingCmdArrayPerId)
		{
			if ($table !== 'tt_content')
			{
				continue;
			}
			foreach ($incomingCmdArrayPerId as $id => $incomingCmdArray)
			{
				if (!is_array($incomingCmdArray))
				{
					continue;
				}
				if (empty($incomingCmdArray['copyToLanguage']))
				{
					continue;
				}
				if (empty($copyMappingArray_merged['tt_content'][$id]))
				{
					continue;
				}
				$copyToLanguage = $incomingCmdArray['copyToLanguage'];
				$newId = $copyMappingArray_merged['tt_content'][$id];
				$data = [
					'tt_content' => [],
				];
				// child in free mode is copied
				$child = $this->database->fetchOneRecord($newId);
				if ($child === null) {
					continue;
				}
				if ($child['tx_maagitcontainer_parent'] > 0)
				{
					$copiedFromChild = $this->database->fetchOneRecord($id);
					// copied from non default language (connectecd mode) children
					if ($copiedFromChild !== null && $copiedFromChild['sys_language_uid'] > 0 && $copiedFromChild['l18n_parent'] > 0)
					{
						// fetch orig container
						$origContainer = $this->database->fetchOneTranslatedRecordByLocalizationParent($copiedFromChild['tx_maagitcontainer_parent'], $copiedFromChild['sys_language_uid']);
						// should never be null
						if ($origContainer !== null)
						{
							$freeModeContainer = $this->database->fetchContainerRecordLocalizedFreeMode((int)$origContainer['uid'], $copyToLanguage);
							if ($freeModeContainer !== null)
							{
								$data['tt_content'][$newId] = ['tx_maagitcontainer_parent' => $freeModeContainer['uid']];
							}
						}
					}
					else
					{
						$freeModeContainer = $this->database->fetchContainerRecordLocalizedFreeMode((int)$child['tx_maagitcontainer_parent'], $copyToLanguage);
						if ($freeModeContainer !== null)
						{
							$data['tt_content'][$newId] = ['tx_maagitcontainer_parent' => $freeModeContainer['uid']];
						}
					}
				}
				if (empty($data['tt_content']))
				{
					continue;
				}
				$localDataHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
				// @extensionScannerIgnoreLine
				$localDataHandler->start($data, [], $dataHandler->BE_USER);
				$localDataHandler->process_datamap();
			}
		}
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}