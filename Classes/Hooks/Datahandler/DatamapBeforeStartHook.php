<?php
namespace Maagit\Maagitcontainer\Hooks\Datahandler;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Hooks
	class:				DatamapBeforeStartHook

	description:		Datamap before start

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class DatamapBeforeStartHook
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $tcaRegistry;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory
	 */
	protected $containerFactory;

	/**
	 * @var \Maagit\Maagitcontainer\Hooks\Datahandler\Database
	 */
	protected $database;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Service\ContainerService
	 */
	protected $containerService;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function __construct(\Maagit\Maagitcontainer\Domain\Factory\ContainerFactory $containerFactory, \Maagit\Maagitcontainer\Hooks\Datahandler\Database $database, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \Maagit\Maagitcontainer\Domain\Service\ContainerService $containerService)
	{
		$this->containerFactory = $containerFactory;
		$this->database = $database;
		$this->tcaRegistry = $tcaRegistry;
		$this->containerService = $containerService;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function processDatamap_beforeStart(\TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler): void
	{
		$dataHandler->datamap = $this->datamapForChildLocalizations($dataHandler->datamap);
		$dataHandler->datamap = $this->datamapForChildrenChangeContainerLanguage($dataHandler->datamap);
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function datamapForChildLocalizations(array $datamap): array
	{
		$datamapForLocalizations = ['tt_content' => []];
		if (!empty($datamap['tt_content']))
		{
			foreach ($datamap['tt_content'] as $id => $data)
			{
				if (isset($data['colPos']))
				{
					$record = $this->database->fetchOneRecord((int)$id);
					if ($record !== null && $record['sys_language_uid'] === 0)
					{
						$translations = $this->database->fetchOverlayRecords($record);
						foreach ($translations as $translation)
						{
							$datamapForLocalizations['tt_content'][$translation['uid']] = [
								'colPos' => $data['colPos'],
							];
							if (isset($data['tx_maagitcontainer_parent']))
							{
								$datamapForLocalizations['tt_content'][$translation['uid']]['tx_maagitcontainer_parent'] = $data['tx_maagitcontainer_parent'];
							}
						}
					}
				}
			}
		}
		if (count($datamapForLocalizations['tt_content']) > 0)
		{
			$datamap['tt_content'] = array_replace($datamap['tt_content'], $datamapForLocalizations['tt_content']);
		}
		return $datamap;
	}

	protected function datamapForChildrenChangeContainerLanguage(array $datamap): array
	{
		$datamapForLocalizations = ['tt_content' => []];
		if (!empty($datamap['tt_content']))
		{
			foreach ($datamap['tt_content'] as $id => $data)
			{
				if (isset($data['sys_language_uid']))
				{
					try
					{
						$container = $this->containerFactory->buildContainer((int)$id);
						$children = $container->getChildRecords();
						foreach ($children as $child)
						{
							if ((int)$child['sys_language_uid'] !== (int)$data['sys_language_uid'])
							{
								$datamapForLocalizations['tt_content'][$child['uid']] = [
									'sys_language_uid' => $data['sys_language_uid'],
								];
							}
						}
					}
					catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
					{
						// nothing todo
					}
				}
			}
		}
		if (count($datamapForLocalizations['tt_content']) > 0)
		{
			$datamap['tt_content'] = array_replace($datamap['tt_content'], $datamapForLocalizations['tt_content']);
		}
		return $datamap;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}