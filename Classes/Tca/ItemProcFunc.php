<?php
namespace Maagit\Maagitcontainer\Tca;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Tca
	class:				ItemProcFunc

	description:		...

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ItemProcFunc
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory
     */
    protected $containerFactory;

    /**
     * @var \TYPO3\CMS\Backend\View\BackendLayoutView
     */
    protected $backendLayoutView;

    /**
     * @var \Maagit\Maagitcontainer\Tca\Registry
     */
    protected $tcaRegistry;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Domain\Factory\ContainerFactory $containerFactory, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \TYPO3\CMS\Backend\View\BackendLayoutView $backendLayoutView)
	{
		$this->containerFactory = $containerFactory;
		$this->tcaRegistry = $tcaRegistry;
		$this->backendLayoutView = $backendLayoutView;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * Gets colPos items to be shown in the forms engine.
	 * This method is called as "itemsProcFunc" with the accordant context
	 * for tt_content.colPos.
	 */
	public function colPos(array &$parameters): void
	{
		$row = $parameters['row'];
		if ($row['tx_maagitcontainer_parent'] > 0)
		{
			try
			{
				$container = $this->containerFactory->buildContainer((int)$row['tx_maagitcontainer_parent']);
				$cType = $container->getCType();
				$grid = $this->tcaRegistry->getGrid($cType);
				if (is_array($grid))
				{
					$items = [];
					foreach ($grid as $rows)
					{
						foreach ($rows as $column)
						{
							// only one item is show, so it is not changeable
							if ((int)$column['colPos'] === (int)$row['colPos'])
							{
								$items[] = [
									'label' => $column['name'],
									'value' => $column['colPos'],
								];
							}
						}
					}
					$parameters['items'] = $items;
					return;
				}
			}
			catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
			{

			}
		}
		
		$this->backendLayoutView->colPosListItemProcFunc($parameters);
	}

	public function txMaagitcontainerParent(array &$parameters): void
	{
		$row = $parameters['row'];
		$items = [];
		if ($row['tx_maagitcontainer_parent'] > 0)
		{
			try
			{
				$container = $this->containerFactory->buildContainer((int)$row['tx_maagitcontainer_parent']);
				$cType = $container->getCType();
				$items[] = [
					'label' => $cType,
					'value' => $row['tx_maagitcontainer_parent'],
				];
			}
			catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
			{
				$items[] = [
					'label' => '-',
					'value' => 0,
				];
			}
		}
		else
		{
			$items[] = [
				'label' => '-',
				'value' => 0,
			];
		}
		$parameters['items'] = $items;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}