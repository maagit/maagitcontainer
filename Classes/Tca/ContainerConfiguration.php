<?php
namespace Maagit\Maagitcontainer\Tca;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Tca
	class:				ContainerConfiguration

	description:		...

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContainerConfiguration
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var string
	 */
	protected $cType = '';

	/**
	 * @var string
	 */
	protected $label = '';

	/**
	 * @var string
	 */
	protected $description = '';

	/**
	 * @var mixed[]
	 */
	protected $grid = [];

	/**
	 * @var string
	 */
	protected $icon = 'EXT:maagitcontainer/Resources/Public/Icons/Extension.png';

	/**
	 * @var string
	 */
	protected $backendTemplate = 'EXT:maagitcontainer/Resources/Private/Templates/Container.html';

	/**
	 * @var string
	 */
	protected $gridTemplate = 'EXT:maagitcontainer/Resources/Private/Templates/Grid.html';

	/**
	 * @var array
	 */
	protected $gridPartialPaths = [
		'EXT:backend/Resources/Private/Partials/',
		'EXT:maagitcontainer/Resources/Private/Partials/',
	];

	protected $gridLayoutPaths = [];

	/**
	 * @var bool
	 */
	protected $saveAndCloseInNewContentElementWizard = true;

	/**
	 * @var bool
	 */
	protected $registerInNewContentElementWizard = true;

	/**
	 * @var string
	 */
	protected $group = 'maagitcontainer';

	/**
	 * @var array
	 */
	protected $defaultValues = [];


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(string $cType, string $label, string $description, array $grid)
	{
		$this->cType = $cType;
		$this->label = $label;
		$this->description = $description;
		$this->grid = $grid;
		$this->gridPartialPaths = [
			'EXT:backend/Resources/Private/Partials/',
			'EXT:maagitcontainer/Resources/Private/Partials/',
		];
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * @param string $icon
	 * @return ContainerConfiguration
	 */
	public function setIcon(string $icon): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->icon = $icon;
		return $this;
	}

	/**
	 * @param string $backendTemplate
	 * @return ContainerConfiguration
	 */
	public function setBackendTemplate(string $backendTemplate): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->backendTemplate = $backendTemplate;
		return $this;
	}

	/**
	 * @param string $gridTemplate
	 * @return ContainerConfiguration
	 */
	public function setGridTemplate(string $gridTemplate): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->gridTemplate = $gridTemplate;
		return $this;
	}

	/**
	 * @param array $gridPartialPaths
	 * @return ContainerConfiguration
	 */
	public function setGridPartialPaths(array $gridPartialPaths): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->gridPartialPaths = $gridPartialPaths;
		return $this;
	}

	/**
	 * @param string $gridPartialPath
	 * @return ContainerConfiguration
	 */
	public function addGridPartialPath(string $gridPartialPath): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->gridPartialPaths[] = $gridPartialPath;
		return $this;
	}

	public function getGridLayoutPaths(): array
	{
		return $this->gridLayoutPaths;
	}

	public function setGridLayoutPaths(array $gridLayoutPaths): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->gridLayoutPaths = $gridLayoutPaths;
		return $this;
	}

	public function addGridLayoutPath(string $gridLayoutPath): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->gridLayoutPaths[] = $gridLayoutPath;
		return $this;
	}

	/**
	 * @param bool $saveAndCloseInNewContentElementWizard
	 * @return ContainerConfiguration
	 */
	public function setSaveAndCloseInNewContentElementWizard(bool $saveAndCloseInNewContentElementWizard): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->saveAndCloseInNewContentElementWizard = $saveAndCloseInNewContentElementWizard;
		return $this;
	}

	/**
	 * @param bool $registerInNewContentElementWizard
	 * @return ContainerConfiguration
	 */
	public function setRegisterInNewContentElementWizard(bool $registerInNewContentElementWizard): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->registerInNewContentElementWizard = $registerInNewContentElementWizard;
		return $this;
	}

	/**
	 * @param string $group
	 * @return ContainerConfiguration
	 */
	public function setGroup(string $group): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->group = $group;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCType(): string
	{
		return $this->cType;
	}

	/**
	 * @return string
	 */
	public function getLabel(): string
	{
		return $this->label;
	}

	/**
	 * @return mixed[]
	 */
	public function getGrid(): array
	{
		return $this->grid;
	}

	/**
	 * @return string[]
	 */
	public function getGridPartialPaths(): array
	{
		return $this->gridPartialPaths;
	}

	/**
	 * @return string
	 */
	public function getGroup(): string
	{
		return $this->group;
	}

	/**
	 * @param array $defaultValues
	 * @return ContainerConfiguration
	 */
	public function setDefaultValues(array $defaultValues): \Maagit\Maagitcontainer\Tca\ContainerConfiguration
	{
		$this->defaultValues = $defaultValues;
		return $this;
	}

	/**
	 * @return mixed[]
	 */
	public function toArray(): array
	{
		return [
			'cType' => $this->cType,
			'icon' => $this->icon,
			'label' => $this->label,
			'description' => $this->description,
			'backendTemplate' => $this->backendTemplate,
			'grid' => $this->grid,
			'gridTemplate' => $this->gridTemplate,
			'gridPartialPaths' => $this->gridPartialPaths,
			'gridLayoutPaths' => $this->gridLayoutPaths,
			'saveAndCloseInNewContentElementWizard' => $this->saveAndCloseInNewContentElementWizard,
			'registerInNewContentElementWizard' => $this->registerInNewContentElementWizard,
			'group' => $this->group,
			'defaultValues' => $this->defaultValues,
		];
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}