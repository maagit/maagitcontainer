<?php
namespace Maagit\Maagitcontainer\Tca;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Tca
	class:				Registry

	description:		...

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Registry implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * @param ContainerConfiguration $containerConfiguration
	 */
	public function configureContainer(\Maagit\Maagitcontainer\Tca\ContainerConfiguration $containerConfiguration): void
	{
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
			'tt_content',
			'CType',
			[
				'label' => $containerConfiguration->getLabel(),
				'value' => $containerConfiguration->getCType(),
				'icon' => $containerConfiguration->getCType(),
				'group' => $containerConfiguration->getGroup(),
			]
		);
		$GLOBALS['TCA']['tt_content']['types'][$containerConfiguration->getCType()]['previewRenderer'] = \Maagit\Maagitcontainer\Backend\Preview\ContainerPreviewRenderer::class;

		foreach ($containerConfiguration->getGrid() as $row)
		{
			foreach ($row as $column)
			{
				if (strpos((string)$column['colPos'], (string)\Maagit\Maagitcontainer\Backend\Grid\ContainerGridColumn::CONTAINER_COL_POS_DELIMITER) !== false)
				{
					trigger_error('delimiter '.(string)\Maagit\Maagitcontainer\Backend\Grid\ContainerGridColumn::CONTAINER_COL_POS_DELIMITER.' cannot be used as colPos (will throw Exception on next major releas)', E_USER_DEPRECATED);
				}
				$GLOBALS['TCA']['tt_content']['columns']['colPos']['config']['items'][] = [
					'label' => $column['name'],
					'value' => $column['colPos'],
				];
			}
		}

		$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes'][$containerConfiguration->getCType()] = $containerConfiguration->getCType();
		$GLOBALS['TCA']['tt_content']['types'][$containerConfiguration->getCType()]['showitem'] = '
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
                    --palette--;;general,
                    header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header.ALT.div_formlabel,
                --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
                    --palette--;;frames,
                    --palette--;;appearanceLinks,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
                    categories,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
                    rowDescription,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
';
	$GLOBALS['TCA']['tt_content']['containerConfiguration'][$containerConfiguration->getCType()] = $containerConfiguration->toArray();
    }

	public function getAllAvailableColumnsColPos(string $cType): array
	{
		$columns = $this->getAvailableColumns($cType);
		$availableColumnsColPos = [];
		foreach ($columns as $column)
		{
			$availableColumnsColPos[] = $column['colPos'];
		}
		return $availableColumnsColPos;
	}

	public function registerIcons(): void
	{
		if (isset($GLOBALS['TCA']['tt_content']['containerConfiguration']) && is_array($GLOBALS['TCA']['tt_content']['containerConfiguration']))
		{
			$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
			foreach ($GLOBALS['TCA']['tt_content']['containerConfiguration'] as $containerConfiguration)
			{
				if (file_exists(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($containerConfiguration['icon'])))
				{
					$provider = \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class;
					if (str_contains($containerConfiguration['icon'], '.svg'))
					{
						$provider = \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class;
					}
					$iconRegistry->registerIcon($containerConfiguration['cType'], $provider, ['source' => $containerConfiguration['icon']]);
				}
				else
				{
					try
					{
						$existingIconConfiguration = $iconRegistry->getIconConfigurationByIdentifier($containerConfiguration['icon']);
						$iconRegistry->registerIcon($containerConfiguration['cType'], $existingIconConfiguration['provider'], $existingIconConfiguration['options']);
                    }
					catch (\TYPO3\CMS\Core\Exception $e)
					{
						
					}
				}
			}
		}
	}

	public function isContainerElement(string $cType): bool
	{
		return !empty($GLOBALS['TCA']['tt_content']['containerConfiguration'][$cType]);
	}

	public function getRegisteredCTypes(): array
	{
		return array_keys((array)($GLOBALS['TCA']['tt_content']['containerConfiguration'] ?? []));
	}

	public function getGrid(string $cType): array
	{
		if (empty($GLOBALS['TCA']['tt_content']['containerConfiguration'][$cType]['grid']))
		{
			return [];
		}
		return $GLOBALS['TCA']['tt_content']['containerConfiguration'][$cType]['grid'];
	}

	public function getGridTemplate(string $cType): ?string
	{
		if (empty($GLOBALS['TCA']['tt_content']['containerConfiguration'][$cType]['gridTemplate']))
		{
			return null;
		}
		return $GLOBALS['TCA']['tt_content']['containerConfiguration'][$cType]['gridTemplate'];
	}

	public function getGridPartialPaths(string $cType): array
	{
		if (empty($GLOBALS['TCA']['tt_content']['containerConfiguration'][$cType]['gridPartialPaths']))
		{
			return [];
		}
		return $GLOBALS['TCA']['tt_content']['containerConfiguration'][$cType]['gridPartialPaths'];
	}

	public function getGridLayoutPaths(string $cType): array
	{
		return $GLOBALS['TCA']['tt_content']['containerConfiguration'][$cType]['gridLayoutPaths'] ?? [];
	}

	public function getColPosName(string $cType, int $colPos): ?string
	{
		$grid = $this->getGrid($cType);
		foreach ($grid as $row)
		{
			foreach ($row as $column)
			{
				if ($column['colPos'] === $colPos)
				{
					return (string)$column['name'];
				}
			}
		}
		return null;
	}

	public function getAvailableColumns(string $cType): array
	{
		$columns = [];
		$grid = $this->getGrid($cType);
		foreach ($grid as $row)
		{
			foreach ($row as $column)
			{
				$columns[] = $column;
			}
		}
		return $columns;
	}

	public function getAllAvailableColumns(): array
	{
		if (empty($GLOBALS['TCA']['tt_content']['containerConfiguration']))
		{
			return [];
		}
		$columns = [];
		foreach ($GLOBALS['TCA']['tt_content']['containerConfiguration'] as $containerConfiguration)
		{
			$grid = $containerConfiguration['grid'];
			foreach ($grid as $row)
			{
				foreach ($row as $column)
				{
					$columns[] = $column;
				}
			}
		}
		return $columns;
	}

	public function getPageTsString(): string
	{
		if (empty($GLOBALS['TCA']['tt_content']['containerConfiguration']))
		{
			return '';
		}
		$pageTs = '';
		// group containers by group
		$groupedByGroup = [];
		$defaultGroup = 'maagitcontainer';
		foreach ($GLOBALS['TCA']['tt_content']['containerConfiguration'] as $cType => $containerConfiguration)
		{
			if ($containerConfiguration['registerInNewContentElementWizard'] === true)
			{
				$group = $containerConfiguration['group'] !== '' ? $containerConfiguration['group'] : $defaultGroup;
				if (empty($groupedByGroup[$group]))
				{
					$groupedByGroup[$group] = [];
				}
				$groupedByGroup[$group][$cType] = $containerConfiguration;
			}
			$pageTs .= LF . 'mod.web_layout.tt_content.preview {
' . $cType . ' = ' . $containerConfiguration['backendTemplate'] . '
}
';
		}
		foreach ($groupedByGroup as $group => $containerConfigurations)
		{
//			$groupLabel = $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['itemGroups'][$group] ?? $group;
$groupLabel = 'MaagIT Container';
            $content = '
mod.wizards.newContentElement.wizardItems.' . $group . '.header = ' . $groupLabel . '
';
			foreach ($containerConfigurations as $cType => $containerConfiguration)
			{
				array_walk($containerConfiguration['defaultValues'], static function (&$item, $key)
				{
					$item = $key . ' = ' . $item;
				});
				$ttContentDefValues = 'CType = ' . $cType . LF . implode(LF, $containerConfiguration['defaultValues']);
				$content .= 'mod.wizards.newContentElement.wizardItems.' . $group . '.elements {
' . $cType . ' {
    title = ' . $containerConfiguration['label'] . '
    description = ' . $containerConfiguration['description'] . '
    iconIdentifier = ' . $cType . '
    tt_content_defValues {
    ' . $ttContentDefValues . '
    }
}
}
';
			}
			$pageTs .= LF . $content;
		}
		return $pageTs;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}