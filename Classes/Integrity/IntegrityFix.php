<?php
namespace Maagit\Maagitcontainer\Integrity;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Integrity
	class:				IntegrityFix

	description:		Integrity Fix

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class IntegrityFix implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Integrity\Database
	 */
	protected $database;

	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $tcaRegistry;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Integrity\Database $database, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry)
	{
		$this->database = $database;
		$this->tcaRegistry = $tcaRegistry;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function deleteChildrenWithWrongPid(\Maagit\Maagitcontainer\Integrity\Error\WrongPidError $wrongPidError): void
	{
		$dataHandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
		$dataHandler->enableLogging = false;
		$childRecord = $wrongPidError->getChildRecord();
		$cmd = ['tt_content' => [$childRecord['uid'] => ['delete' => 1]]];
		$dataHandler->start([], $cmd);
		$dataHandler->process_cmdmap();
	}

	public function changeContainerParentToDefaultLanguageContainer(\Maagit\Maagitcontainer\Integrity\Error\ChildInTranslatedContainerError $e): void
	{
		$translatedContainer = $e->getContainerRecord();
		$child = $e->getChildRecord();
		$l18nParentOfContainer = $translatedContainer['l18n_parent'];
		$queryBuilder = $this->database->getQueryBuilder();
		$queryBuilder->update('tt_content')
			->set('tx_maagitcontainer_parent', $l18nParentOfContainer)
			->where(
				$queryBuilder->expr()->eq(
					'uid',
					$queryBuilder->createNamedParameter($child['uid'], \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
	}

	/**
	 * @param WrongL18nParentError[] $errors
	 */
	public function languageMode(array $errors): void
	{
		$cTypes = $this->tcaRegistry->getRegisteredCTypes();
		$defaultContainerRecords = $this->database->getContainerRecords($cTypes);
		$containerRecords = [];
		// uniq container records
		foreach ($errors as $error)
		{
			$containerRecord = $error->getContainerRecord();
			$containerRecords[$containerRecord['uid']] = $containerRecord;
		}
		foreach ($containerRecords as $containerRecord)
		{
			if (!isset($defaultContainerRecords[$containerRecord['l18n_parent']]))
			{
				// should not happen
				continue;
			}
			$defaultContainerRecord = $defaultContainerRecords[$containerRecord['l18n_parent']];
			$columns = $this->tcaRegistry->getAvailableColumns($defaultContainerRecord['CType']);
			foreach ($columns as $column)
			{
				$childRecords = $this->database->getChildrenByContainerAndColPos($containerRecord['uid'], (int)$column['colPos'], $containerRecord['sys_language_uid']);
				// some children may have corrent container parent set
				//$childRecords = array_merge($childRecords, $this->database->getChildrenByContainer($defaultContainerRecord['uid'], $containerRecord['sys_language_uid']));
				$defaultChildRecords = $this->database->getChildrenByContainerAndColPos($defaultContainerRecord['uid'], (int)$column['colPos'], $defaultContainerRecord['sys_language_uid']);
				if (count($childRecords) <= count($defaultChildRecords))
				{
					// connect children
					for ($i = 0; $i < count($childRecords); $i++)
					{
						$childRecord = $childRecords[$i];
						$defaultChildRecord = $defaultChildRecords[$i];
						$queryBuilder = $this->database->getQueryBuilder();
						$stm = $queryBuilder->update('tt_content')
							->set('tx_maagitcontainer_parent', $defaultContainerRecord['uid'])
							->set('l18n_parent', $defaultChildRecord['uid'])
							->where(
								$queryBuilder->expr()->eq(
									'uid',
									$queryBuilder->createNamedParameter($childRecord['uid'], \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
								)
							);
						if ((int)$childRecord['l10n_source'] === 0)
						{
							// i think this is always true
							$stm->set('l10n_source', $defaultChildRecord['uid']);
						}
						$stm->executeQuery();
					}
				}
				// disconnect container ?
			}
		}
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}