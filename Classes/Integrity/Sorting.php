<?php
namespace Maagit\Maagitcontainer\Integrity;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Integrity
	class:				Sorting

	description:		Sorting

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Sorting implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Integrity\Database
	 */
	protected $database;

	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $tcaRegistry;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory
	 */
	protected $containerFactory;

	protected $errors = [];


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Integrity\Database $database, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory $containerFactory)
	{
		$this->database = $database;
		$this->tcaRegistry = $tcaRegistry;
		$this->containerFactory = $containerFactory;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function run(bool $dryRun = true): array
	{
		$cTypes = $this->tcaRegistry->getRegisteredCTypes();
		$containerRecords = $this->database->getContainerRecords($cTypes);
		$containerRecords = array_merge($containerRecords, $this->database->getContainerRecordsFreeMode($cTypes));
		$colPosByCType = [];
		foreach ($cTypes as $cType)
		{
			$columns = $this->tcaRegistry->getAvailableColumns($cType);
			$colPosByCType[$cType] = [];
			foreach ($columns as $column)
			{
				$colPosByCType[$cType][] = $column['colPos'];
			}
		}
		$this->fixChildrenSorting($containerRecords, $colPosByCType, $dryRun);
		return $this->errors;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function fixChildrenSortingUpdateRequired(\Maagit\Maagitcontainer\Domain\Model\Container $container, array $colPosByCType): bool
	{
		$containerRecord = $container->getContainerRecord();
		$prevSorting = $containerRecord['sorting'];
		foreach ($colPosByCType[$containerRecord['CType']] as $colPos)
		{
			$children = $container->getChildrenByColPos($colPos);
			foreach ($children as $child)
			{
				if ($child['sorting'] <= $prevSorting)
				{
					$this->errors[] = 'container uid: '.$containerRecord['uid'].', pid '.$containerRecord['pid'].' must be fixed';
					return true;
				}
				$prevSorting = $child['sorting'];
			}
		}
		return false;
	}

	protected function fixChildrenSorting(array $containerRecords, array $colPosByCType, bool $dryRun): void
	{
		$datahandler = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\DataHandler::class);
		$datahandler->enableLogging = false;
		foreach ($containerRecords as $containerRecord)
		{
			try
			{
				$container = $this->containerFactory->buildContainer($containerRecord['uid']);
			}
			catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
			{
				// should not happend
				continue;
			}
			if ($this->fixChildrenSortingUpdateRequired($container, $colPosByCType) === false || $dryRun === true)
			{
				continue;
			}
			$prevChild = null;
			foreach ($colPosByCType[$containerRecord['CType']] as $colPos)
			{
				$children = $container->getChildrenByColPos($colPos);
				if (empty($children))
				{
					continue;
				}
				foreach ($children as $child)
				{
					if ($prevChild === null)
					{
						$cmdmap = [
							'tt_content' => [
								$child['uid'] => [
									'move' => [
										'action' => 'paste',
										'target' => $container->getPid(),
										'update' => [
											'colPos' => $container->getUid().'-'.$child['colPos'],
											'sys_language_uid' => $containerRecord['sys_language_uid'],
										],
									],
								],
							],
						];
						$datahandler->start([], $cmdmap);
						$datahandler->process_datamap();
						$datahandler->process_cmdmap();
					}
					else
					{
						$cmdmap = [
							'tt_content' => [
								$child['uid'] => [
									'move' => [
										'action' => 'paste',
										'target' => -$prevChild['uid'],
										'update' => [
											'colPos' => $container->getUid().'-'.$child['colPos'],
											'sys_language_uid' => $containerRecord['sys_language_uid'],
										],
									],
								],
							],
						];
						$datahandler->start([], $cmdmap);
						$datahandler->process_datamap();
						$datahandler->process_cmdmap();
					}
					$prevChild = $child;
				}
			}
		}
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}