<?php
namespace Maagit\Maagitcontainer\Error;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Error
	class:				WrongL18nParentError

	description:		WrongL18nParentError

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class WrongL18nParentError implements \Maagit\Maagitcontainer\Error\ErrorInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	private const IDENTIFIER = 'WrongL18nParentError';

	/**
	 * @var array
	 */
	protected $childRecord;

	/**
	 * @var array
	 */
	protected $containerRecord;

	/**
	 * @var string
	 */
	protected $errorMessage;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
	 * @param array $childRecord
	 * @param array $containerRecord
	 */
	public function __construct(array $childRecord, array $containerRecord)
	{
		$this->childRecord = $childRecord;
		$this->containerRecord = $containerRecord;
		$this->errorMessage = self::IDENTIFIER . ': container child with uid ' . $childRecord['uid'] .
            ' (page: ' . $childRecord['pid'] . ' language: ' . $childRecord['sys_language_uid'] . ')' .
            ' has l18n_parent ' . $childRecord['l18n_parent']
            . ' but tx_maagitcontainer_parent ' . $childRecord['tx_maagitcontainer_parent']
            . ' has l18n_parent ' . $containerRecord['l18n_parent'] .
            ' (page: ' . $containerRecord['pid'] . ' language: ' . $containerRecord['sys_language_uid'] . ')';
    }


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
	 * @return string
	 */
	public function getErrorMessage(): string
	{
		return $this->errorMessage;
	}

	/**
	 * @return int
	 */
	public function getSeverity(): int
	{
		return AbstractMessage::ERROR;
	}

	/**
	 * @return array
	 */
	public function getChildRecord(): array
	{
		return $this->childRecord;
	}

	/**
	 * @return array
	 */
	public function getContainerRecord(): array
	{
		return $this->containerRecord;
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}