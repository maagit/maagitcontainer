<?php
namespace Maagit\Maagitcontainer\Integrity;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Integrity
	class:				Database

	description:		Database

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Database implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	private $fields = ['uid', 'pid', 'sys_language_uid', 'CType', 'l18n_parent', 't3_origuid', 'colPos', 'tx_maagitcontainer_parent', 'l10n_source', 'hidden', 'sorting'];


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	protected function getQueryBuilder(): \TYPO3\CMS\Core\Database\Query\QueryBuilder
	{
		$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('tt_content');
		$queryBuilder->getRestrictions()->removeAll()->add(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction::class));
		return $queryBuilder;
	}

	public function getNonDefaultLanguageContainerRecords(array $cTypes): array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder
			->select(...$this->fields)
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'CType',
					$queryBuilder->createNamedParameter($cTypes, \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
				),
				$queryBuilder->expr()->gt(
					'sys_language_uid',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$results = $stm->fetchAllAssociative();
		$rows = [];
		foreach ($results as $result)
		{
			$rows[$result['uid']] = $result;
		}
		return $rows;
	}

	public function getNonDefaultLanguageContainerChildRecords(): array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder
			->select(...$this->fields)
			->from('tt_content')
			->where(
				$queryBuilder->expr()->gt(
					'tx_maagitcontainer_parent',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->gt(
					'sys_language_uid',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$results = $stm->fetchAllAssociative();
		$rows = [];
		foreach ($results as $result)
		{
			$rows[$result['uid']] = $result;
		}
		return $rows;
	}

	public function getChildrenByContainerAndColPos(int $containerId, int $colPos, int $languageId): array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder
			->select(...$this->fields)
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'tx_maagitcontainer_parent',
					$queryBuilder->createNamedParameter($containerId, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter($languageId, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->eq(
					'colPos',
					$queryBuilder->createNamedParameter($colPos, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->orderBy('sorting')
			->executeQuery();
		return (array)$stm->fetchAllAssociative();
	}

	public function getContainerRecords(array $cTypes): array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder
			->select(...$this->fields)
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'CType',
					$queryBuilder->createNamedParameter($cTypes, \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
				),
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$results = $stm->fetchAllAssociative();
		$rows = [];
		foreach ($results as $result)
		{
			$rows[$result['uid']] = $result;
		}
		return $rows;
	}

	public function getContainerRecordsFreeMode(array $cTypes): array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder
			->select(...$this->fields)
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'CType',
					$queryBuilder->createNamedParameter($cTypes, \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
				),
				$queryBuilder->expr()->neq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->eq(
					'l18n_parent',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$results = $stm->fetchAllAssociative();
		$rows = [];
		foreach ($results as $result)
		{
			$rows[$result['uid']] = $result;
		}
		return $rows;
	}

	public function getContainerChildRecords(): array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder
			->select(...$this->fields)
			->from('tt_content')
			->where(
				$queryBuilder->expr()->gt(
					'tx_maagitcontainer_parent',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$results = $stm->fetchAllAssociative();
		$rows = [];
		foreach ($results as $result)
		{
			$rows[$result['uid']] = $result;
		}
		return $rows;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}