<?php
namespace Maagit\Maagitcontainer\Integrity;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Integrity
	class:				Integrity

	description:		Integrity

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Integrity implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Integrity\Database
	 */
	protected $database;

	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $tcaRegistry;

	/**
	 * @var string[][]
	 */
	protected $res = [
		'errors' => [],
		'warnings' => [],
	];


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Integrity\Database $database, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry)
	{
		$this->database = $database;
		$this->tcaRegistry = $tcaRegistry;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function run(): array
	{
		$cTypes = $this->tcaRegistry->getRegisteredCTypes();
		$colPosByCType = [];
		foreach ($cTypes as $cType)
		{
			$columns = $this->tcaRegistry->getAvailableColumns($cType);
			$colPosByCType[$cType] = [];
			foreach ($columns as $column)
			{
				$colPosByCType[$cType][] = $column['colPos'];
			}
		}
		$this->defaultLanguageRecords($cTypes, $colPosByCType);
		$this->nonDefaultLanguageRecords($cTypes, $colPosByCType);
		return $this->res;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	private function nonDefaultLanguageRecords(array $cTypes, array $colPosByCType): void
	{
		$nonDefaultLanguageChildRecords = $this->database->getNonDefaultLanguageContainerChildRecords();
		$nonDefaultLangaugeContainerRecords = $this->database->getNonDefaultLanguageContainerRecords($cTypes);
		$defaultLanguageContainerRecords = $this->database->getContainerRecords($cTypes);
		foreach ($nonDefaultLanguageChildRecords as $nonDefaultLanguageChildRecord)
		{
			if ($nonDefaultLanguageChildRecord['l18n_parent'] > 0)
			{
				// connected mode
				// tx_maagitcontainer_parent should be default container record uid
				if (!isset($defaultLanguageContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']]))
				{
					if (isset($nonDefaultLangaugeContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']]))
					{
						$containerRecord = $nonDefaultLangaugeContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']];
						if ($containerRecord['sys_language_uid'] === $nonDefaultLanguageChildRecord['sys_language_uid'] && $containerRecord['l18n_parent'] > 0)
						{
							$this->res['errors'][] = new \Maagit\Maagitcontainer\Integrity\Error\ChildInTranslatedContainerError($nonDefaultLanguageChildRecord, $containerRecord);
						}
						else
						{
							$this->res['warnings'][] = new \Maagit\Maagitcontainer\Integrity\Error\NonExistingParentWarning($nonDefaultLanguageChildRecord);
						}
					}
					else
					{
						$this->res['warnings'][] = new \Maagit\Maagitcontainer\Integrity\Error\NonExistingParentWarning($nonDefaultLanguageChildRecord);
					}
				}
				elseif (isset($nonDefaultLangaugeContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']]))
				{
					$containerRecord = $nonDefaultLangaugeContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']];
					$this->res['errors'][] = new \Maagit\Maagitcontainer\Integrity\Error\WrongL18nParentError($nonDefaultLanguageChildRecord, $containerRecord);
				}
			}
			else
			{
				// free mode, can be created direct, or by copyToLanguage
				// tx_maagitcontainer_parent should be nonDefaultLanguage container record uid
				if (isset($defaultLanguageContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']]))
				{
					$containerRecord = $defaultLanguageContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']];
					if ($containerRecord['pid'] !== $nonDefaultLanguageChildRecord['pid'])
					{
						$this->res['errors'][] = new \Maagit\Maagitcontainer\Integrity\Error\WrongPidError($nonDefaultLanguageChildRecord, $containerRecord);
					}
					$this->res['warnings'][] = new \Maagit\Maagitcontainer\Integrity\Error\WrongLanguageWarning($nonDefaultLanguageChildRecord, $containerRecord);
				}
				elseif (!isset($nonDefaultLangaugeContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']]))
				{
					$this->res['warnings'][] = new \Maagit\Maagitcontainer\Integrity\Error\NonExistingParentWarning($nonDefaultLanguageChildRecord);
				}
				else
				{
					$containerRecord = $nonDefaultLangaugeContainerRecords[$nonDefaultLanguageChildRecord['tx_maagitcontainer_parent']];
					if ($containerRecord['pid'] !== $nonDefaultLanguageChildRecord['pid'])
					{
						$this->res['errors'][] = new \Maagit\Maagitcontainer\Integrity\Error\WrongPidError($nonDefaultLanguageChildRecord, $containerRecord);
					}
					if ($containerRecord['sys_language_uid'] !== $nonDefaultLanguageChildRecord['sys_language_uid'])
					{
						$this->res['errors'][] = new \Maagit\Maagitcontainer\Integrity\Error\WrongL18nParentError($nonDefaultLanguageChildRecord, $containerRecord);
					}
					if (!in_array($nonDefaultLanguageChildRecord['colPos'], $colPosByCType[$containerRecord['CType']]))
					{
						$this->res['warnings'][] = new \Maagit\Maagitcontainer\Integrity\Error\UnusedColPosWarning($nonDefaultLanguageChildRecord, $containerRecord);
					}
					if ($containerRecord['l18n_parent'] > 0)
					{
						$this->res['errors'][] = new \Maagit\Maagitcontainer\Integrity\Error\WrongL18nParentError($nonDefaultLanguageChildRecord, $containerRecord);
					}
				}
			}
		}
	}

	private function defaultLanguageRecords(array $cTypes, array $colPosByCType): void
	{
		$containerRecords = $this->database->getContainerRecords($cTypes);
		$containerChildRecords = $this->database->getContainerChildRecords();
		foreach ($containerChildRecords as $containerChildRecord)
		{
			if (!isset($containerRecords[$containerChildRecord['tx_maagitcontainer_parent']]))
			{
				// can happen when container CType is changed
				$this->res['warnings'][] = new \Maagit\Maagitcontainer\Integrity\Error\NonExistingParentWarning($containerChildRecord);
			}
			else
			{
				$containerRecord = $containerRecords[$containerChildRecord['tx_maagitcontainer_parent']];
				if ($containerRecord['pid'] !== $containerChildRecord['pid'])
				{
					$this->res['errors'][] = new \Maagit\Maagitcontainer\Integrity\Error\WrongPidError($containerChildRecord, $containerRecord);
				}
				if (!in_array($containerChildRecord['colPos'], $colPosByCType[$containerRecord['CType']]))
				{
					// can happen when container CType is changed
					$this->res['warnings'][] = new \Maagit\Maagitcontainer\Integrity\Error\UnusedColPosWarning($containerChildRecord, $containerRecord);
				}
			}
		}
	}
}