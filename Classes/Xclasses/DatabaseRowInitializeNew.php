<?php
namespace Maagit\Maagitcontainer\Xclasses;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Xclasses
	class:				DatabaseRowInitializeNew

	description:		...

	created:			2024-02-15
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2024-02-15	Urs Maag		Initial version
						2024-09-19	Urs Maag		Check on existing key to avoid
													errors from other content elements

------------------------------------------------------------------------------------- */


class DatabaseRowInitializeNew extends \TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseRowInitializeNew
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * If a neighbor row is given (if vanillaUid was negative), field can be initialized with values
     * from neighbor for fields registered in TCA['ctrl']['useColumnsForDefaultValues'].
     *
     * @param array $result Result array
     * @return array Modified result array
     */
    protected function setDefaultsFromNeighborRow(array $result)
    {
		$result = parent::setDefaultsFromNeighborRow($result);
		if (!empty($result['neighborRow']) && array_key_exists('tx_maagitcontainer_parent', $result['neighborRow'])) {
			if ($result['vanillaUid'] < 0 && $result['neighborRow']['tx_maagitcontainer_parent'] == 0)
			{
				$result['databaseRow']['tx_maagitcontainer_parent'] = abs($result['vanillaUid']);	
			}
		}
		return $result;
    }

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}