<?php
namespace Maagit\Maagitcontainer\Command;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Command
	class:				SortingCommand

	description:		Sorting command

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class SortingCommand extends \Symfony\Component\Console\Command\Command
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Integrity\Sorting
	 */
	protected $sorting;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Integrity\Sorting $sorting, string $name = null)
	{
		parent::__construct($name);
		$this->sorting = $sorting;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    public function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output): int
	{
		$dryrun = (bool)$input->getArgument('dryrun');
		if ($input->getOption('apply') === true)
		{
			$dryrun = false;
		}
		\TYPO3\CMS\Core\Core\Bootstrap::initializeBackendAuthentication();
		\TYPO3\CMS\Core\Core\Bootstrap::initializeLanguageObject();
		// @extensionScannerIgnoreLine
		$errors = $this->sorting->run($dryrun);
		foreach ($errors as $error)
		{
			$output->writeln($error);
		}
		if (empty($errors)) {
			$output->writeln('migration finished');
		}
		return 0;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function configure()
	{
		$this->addArgument('dryrun', \Symfony\Component\Console\Input\InputArgument::OPTIONAL, 'do not execute queries', true);
		$this->addOption('apply', null, \Symfony\Component\Console\Input\InputOption::VALUE_NONE, 'apply migration');
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}