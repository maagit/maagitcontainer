<?php
namespace Maagit\Maagitcontainer\Backend\Grid;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Backend
	class:				ContainerGridColumn

	description:		Backend Grid

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContainerGridColumn extends \TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumn
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	public const CONTAINER_COL_POS_DELIMITER = 999990;

    protected $container;

    protected $allowNewContentElements = true;

    protected $newContentElementAtTopTarget;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function __construct(\TYPO3\CMS\Backend\View\PageLayoutContext $context, array $columnDefinition, \Maagit\Maagitcontainer\Domain\Model\Container $container, int $newContentElementAtTopTarget, bool $allowNewContentElements = true)
	{
		parent::__construct($context, $columnDefinition);
		$this->container = $container;
		$this->allowNewContentElements = $allowNewContentElements;
		$this->newContentElementAtTopTarget = $newContentElementAtTopTarget;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function getDataColPos(): int
	{
		return (int)($this->getContainerUid().self::CONTAINER_COL_POS_DELIMITER.$this->getColumnNumber());
	}

	public function getContainerUid(): int
	{
		return $this->container->getUidOfLiveWorkspace();
	}

	public function getTitle(): string
	{
		return (string)$this->getLanguageService()->sL($this->getColumnName());
	}

	public function getAllowNewContent(): bool
	{
		// @extensionScannerIgnoreLine
		if ($this->container->getLanguage() > 0 && $this->container->isConnectedMode()) {
			return false;
		}
		return $this->allowNewContentElements;
	}

	public function isActive(): bool
	{
		// yes we are active
		return true;
	}

	public function getNewContentUrl(): string
	{
		$pageId = $this->context->getPageId();
		$urlParameters = [
			'id' => $pageId,
			// @extensionScannerIgnoreLine
			'sys_language_uid' => $this->container->getLanguage(),
			'colPos' => $this->getColumnNumber(),
			'tx_maagitcontainer_parent' => $this->container->getUidOfLiveWorkspace(),
			'uid_pid' => $this->newContentElementAtTopTarget,
			'defVals' => [
				'tt_content' => [
					'colPos' => $this->getColumnNumber(),
					// @extensionScannerIgnoreLine
					'sys_language_uid' => $this->context->getSiteLanguage()->getLanguageId(),
				]
			],	
			'returnUrl' => \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('REQUEST_URI'),
		];
		$uriBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Backend\Routing\UriBuilder::class);
		return (string)$uriBuilder->buildUriFromRoute('new_content_element_wizard', $urlParameters);
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}