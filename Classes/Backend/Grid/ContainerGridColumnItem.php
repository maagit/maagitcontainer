<?php
namespace Maagit\Maagitcontainer\Backend\Grid;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Backend
	class:				ContainerGridColumnItem

	description:		Container grid column item

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContainerGridColumnItem extends \TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumnItem
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	protected $container;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\TYPO3\CMS\Backend\View\PageLayoutContext $context, \TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumn $column, array $record, \Maagit\Maagitcontainer\Domain\Model\Container $container)
	{
		parent::__construct($context, $column, $record);
		$this->container = $container;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function getAllowNewContent(): bool
	{
		// @extensionScannerIgnoreLine
		if ($this->container->getLanguage() > 0 && $this->container->isConnectedMode()) {
			return false;
		}
		return true;
	}

	public function getWrapperClassName(): string
	{
		$wrapperClassNames = [];
		if ($this->isDisabled()) {
			$wrapperClassNames[] = 't3-page-ce-hidden t3js-hidden-record';
		}
		// we do not need a "t3-page-ce-warning" class because we are build from Container
		return implode(' ', $wrapperClassNames);
	}

	public function getNewContentAfterUrl(): string
	{
		$pageId = $this->context->getPageId();
		$urlParameters = [
			'id' => $pageId,
			// @extensionScannerIgnoreLine
			'sys_language_uid' => $this->container->getLanguage(),
			'colPos' => $this->column->getColumnNumber(),
			'tx_maagitcontainer_parent' => $this->container->getUidOfLiveWorkspace(),
			'uid_pid' => -$this->container->getUidOfLiveWorkspace(),
			'returnUrl' => \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('REQUEST_URI'),
		];
		$uriBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Backend\Routing\UriBuilder::class);
		return (string)$uriBuilder->buildUriFromRoute('new_content_element_wizard', $urlParameters);
    }

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}