<?php
namespace Maagit\Maagitcontainer\Backend\Preview;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Backend
	class:				ContainerPreviewRenderer

	description:		Container Preview

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version
						2024-09-19	Urs Maag		Method "renderPageModulePreviewContent"
													replace creating TemplateView with new
													ViewFactoryInterface

------------------------------------------------------------------------------------- */


class ContainerPreviewRenderer extends \TYPO3\CMS\Backend\Preview\StandardContentPreviewRenderer
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var \Maagit\Maagitcontainer\Tca\Registry
     */
    protected $tcaRegistry;

    /**
     * @var \Maagit\Maagitcontainer\Domain\Factory\PageView\Backend\ContainerFactory
     */
    protected $containerFactory;

    /**
     * @var \Maagit\Maagitcontainer\Domain\Service\ContainerService
     */
    protected $containerService;

	/**
	 * @var \TYPO3\CMS\Core\View\ViewFactoryInterface
	 */
	protected $viewFactory;

	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \Maagit\Maagitcontainer\Domain\Factory\PageView\Backend\ContainerFactory $containerFactory, \Maagit\Maagitcontainer\Domain\Service\ContainerService $containerService)
	{
		$this->tcaRegistry = $tcaRegistry ?? \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Tca\Registry::class);
		$this->containerFactory = $containerFactory ?? \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Domain\Factory\PageView\Backend\ContainerFactory::class);
		$this->containerService = $containerService ?? \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Domain\Service\ContainerService::class);
		$this->viewFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\View\ViewFactoryInterface::class);
    }


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    public function renderPageModulePreviewContent(\TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumnItem $item): string
    {
		$content = parent::renderPageModulePreviewContent($item);
		// @extensionScannerIgnoreLine
		$context = $item->getContext();
		$record = $item->getRecord();
		$grid = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Backend\View\BackendLayout\Grid\Grid::class, $context);
		try
		{
			$container = $this->containerFactory->buildContainer((int)$record['uid']);
		}
		catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
		{
			// not a container
			return $content;
		}
		$containerGrid = $this->tcaRegistry->getGrid($record['CType']);
		foreach ($containerGrid as $row => $cols) {
			$rowObject = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Backend\View\BackendLayout\Grid\GridRow::class, $context);
            foreach ($cols as $col) {
                $newContentElementAtTopTarget = $this->containerService->getNewContentElementAtTopTargetInColumn($container, $col['colPos']);
				$columnObject = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Backend\Grid\ContainerGridColumn::class, $context, $col, $container, $newContentElementAtTopTarget);
				$rowObject->addColumn($columnObject);
				if (isset($col['colPos']))
				{
					$records = $container->getChildrenByColPos($col['colPos']);
					foreach ($records as $contentRecord)
					{
						$columnItem = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Backend\Grid\ContainerGridColumnItem::class, $context, $columnObject, $contentRecord, $container);
						$columnObject->addItem($columnItem);
					}
				}
			}
			$grid->addRow($rowObject);
		}

		$gridTemplate = $this->tcaRegistry->getGridTemplate($record['CType']);
		$partialRootPaths = $this->tcaRegistry->getGridPartialPaths($record['CType']);
		$layoutRootPaths = $this->tcaRegistry->getGridLayoutPaths($record['CType']);
		
		$viewFactoryData = new \TYPO3\CMS\Core\View\ViewFactoryData(
			partialRootPaths: $partialRootPaths,
			layoutRootPaths: $layoutRootPaths,
			templatePathAndFilename: $gridTemplate
		);		
		$view = $this->viewFactory->create($viewFactoryData);

		$view->assign('hideRestrictedColumns', (bool)(\TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($context->getPageId())['mod.']['web_layout.']['hideRestrictedCols'] ?? false));
		$GLOBALS['LANG']->sL('LLL:EXT:my_extension/Resources/Private/Language/db.xlf:my_label');
		$view->assign('newContentTitle', $this->getLanguageService()->sL('newContentElement'));
		$view->assign('newContentTitleShort', $this->getLanguageService()->sL('content'));
		$view->assign('allowEditContent', $this->getBackendUser()->check('tables_modify', 'tt_content'));
		$view->assign('containerGrid', $grid);
		$view->assign('containerRecord', $container->getContainerRecord());
		$view->assign('containerUid', $container->getContainerRecord()['uid']);
		$rendered = $view->render();

		return $content . $rendered;
    }
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}