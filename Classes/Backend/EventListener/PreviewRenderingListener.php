<?php
namespace Maagit\Maagitcontainer\Backend\EventListener;
use TYPO3\CMS\Core\Attribute\AsEventListener;

#[AsEventListener(
    identifier: 'maagitcontainer/preview-rendering-maagitcontainer-ctype',
)]
final readonly class PreviewRenderingListener
{
    public function __invoke(\TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent $event): void
    {
        if ($event->getTable() !== 'tt_content') {
            return;
        }
		
		$tcaRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Tca\Registry::class);
		$grid = $tcaRegistry->getGrid($event->getRecord()['CType']);
        if (!empty($grid))
		{
			$gridColumn = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
				\TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumn::class,
				$event->getPageLayoutContext(),
				$grid,
				$event->getTable()
			);

			$gridColumnItem = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
				\TYPO3\CMS\Backend\View\BackendLayout\Grid\GridColumnItem::class, 
				$event->getPageLayoutContext(),
				$gridColumn,
				$event->getRecord(),
				$event->getTable()
			);
			
			$containerPreviewRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Backend\Preview\ContainerPreviewRenderer::class);
			$content = $containerPreviewRenderer->renderPageModulePreviewContent($gridColumnItem);
			$event->setPreviewContent($content);
        }
    }
}
