<?php
namespace Maagit\Maagitcontainer\DataProcessing;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			DataProcessing
	class:				ContainerProcessor

	description:		Container processor

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContainerProcessor implements \TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\PageView\Frontend\ContainerFactory
	 */
	protected $containerFactory;

	/**
	 * @var \TYPO3\CMS\Frontend\ContentObject\ContentDataProcessor
	 */
	protected $contentDataProcessor;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Domain\Factory\PageView\Frontend\ContainerFactory $containerFactory, \TYPO3\CMS\Frontend\ContentObject\ContentDataProcessor $contentDataProcessor)
	{
		$this->containerFactory = $containerFactory;
		$this->contentDataProcessor = $contentDataProcessor;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function process(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData): array
	{
		if (isset($processorConfiguration['if.']) && !$cObj->checkIf($processorConfiguration['if.']))
		{
			return $processedData;
		}
		if ($processorConfiguration['contentId.'] ?? false)
		{
			$contentId = (int)$cObj->stdWrap($processorConfiguration['contentId'], $processorConfiguration['contentId.']);
		}
		elseif ($processorConfiguration['contentId'] ?? false)
		{
			$contentId = (int)$processorConfiguration['contentId'];
		}
		else
		{
			// @extensionScannerIgnoreLine
			$contentId = (int)$cObj->data['uid'];
		}

		try
		{
			$container = $this->containerFactory->buildContainer($contentId);
		}
		catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
		{
			// do nothing
			return $processedData;
		}

		$colPos = (int)$cObj->stdWrapValue('colPos', $processorConfiguration);
		if (empty($colPos))
		{
			$allColPos = $container->getChildrenColPos();
			foreach ($allColPos as $colPos)
			{
				$processedData = $this->processColPos($cObj, $container, $colPos, 'children_'.$colPos, $processedData, $processorConfiguration);
			}
		}
		else
		{
			$as = $cObj->stdWrapValue('as', $processorConfiguration, 'children');
			$processedData = $this->processColPos($cObj, $container, $colPos, $as, $processedData, $processorConfiguration);
		}
		return $processedData;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function processColPos(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $cObj, \Maagit\Maagitcontainer\Domain\Model\Container $container, int $colPos, string $as, array $processedData, array $processorConfiguration): array
	{
		$timeTracker = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\TimeTracker\TimeTracker::class);
		$children = $container->getChildrenByColPos($colPos);
		$contentRecordRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\ContentObject\RecordsContentObject::class, $timeTracker);
		$contentRecordRenderer->setRequest($this->getRequest());
		$contentRecordRenderer->setContentObjectRenderer($cObj);
		$conf = [
			'tables' => 'tt_content',
		];
		foreach ($children as &$child)
		{
			if ($child['l18n_parent'] > 0)
			{
				$conf['source'] = $child['l18n_parent'];
			}
			else
			{
				$conf['source'] = $child['uid'];
			}
			if ($child['t3ver_oid'] > 0)
			{
				$conf['source'] = $child['t3ver_oid'];
			}
			$child['renderedContent'] = $cObj->render($contentRecordRenderer, $conf);
			/** @var ContentObjectRenderer $recordContentObjectRenderer */
			$recordContentObjectRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer::class);
			$recordContentObjectRenderer->start($child, 'tt_content');
			$child = $this->contentDataProcessor->process($recordContentObjectRenderer, $processorConfiguration, $child);
		}
		$processedData[$as] = $children;
		return $processedData;
	}

	protected function getRequest(): \Psr\Http\Message\ServerRequestInterface
	{
		return $GLOBALS['TYPO3_REQUEST'];
	}

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}