<?php
namespace Maagit\Maagitcontainer\Domain\Factory;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Factory
	class:				ContainerFactory

	description:		Container Factory

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContainerFactory implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\Database
	 */
	protected $database;

	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $tcaRegistry;

	/**
	 * @var int
	 */
	protected $workspaceId = 0;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Domain\Factory\Database $database, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \TYPO3\CMS\Core\Context\Context $context)
	{
		$this->database = $database;
		$this->tcaRegistry = $tcaRegistry;
		$this->workspaceId = (int)$context->getPropertyFromAspect('workspace', 'id');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function buildContainer(int $uid): \Maagit\Maagitcontainer\Domain\Model\Container
	{
		$record = $this->containerByUid($uid);
		if ($record === null)
		{
			throw new \Maagit\Maagitcontainer\Domain\Factory\Exception('cannot fetch record with uid ' . $uid, 1576572850);
		}
		if (!$this->tcaRegistry->isContainerElement($record['CType']))
		{
			throw new \Maagit\Maagitcontainer\Domain\Factory\Exception('not a container element with uid ' . $uid, 1576572851);
		}

		$defaultRecord = null;
		$language = (int)$record['sys_language_uid'];
		if ($language > 0)
		{
			$defaultRecord = $this->defaultContainer($record);
			if ($defaultRecord === null)
			{
				// free mode
				$childRecords = $this->children($record, $language);
			}
			else
			{
				// connected mode
				$defaultRecords = $this->children($defaultRecord, 0);
				$childRecords = $this->localizedRecordsByDefaultRecords($defaultRecords, $language);
			}
		}
		else
		{
			$childRecords = $this->children($record, $language);
		}
		$childRecordByColPosKey = $this->recordsByColPosKey($childRecords);
		if ($defaultRecord === null)
		{
			$container = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Domain\Model\Container::class, $record, $childRecordByColPosKey, $language);
		}
		else
		{
			$container = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Domain\Model\Container::class, $defaultRecord, $childRecordByColPosKey, $language);
		}
		return $container;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function containerByUid(int $uid): ?array
	{
		return $this->database->fetchOneRecord($uid);
	}

	protected function defaultContainer(array $localizedContainer): ?array
	{
		return $this->database->fetchOneDefaultRecord($localizedContainer);
	}

	protected function localizedRecordsByDefaultRecords(array $defaultRecords, int $language): array
	{
		$localizedRecords = $this->database->fetchOverlayRecords($defaultRecords, $language);
		$childRecords = $this->sortLocalizedRecordsByDefaultRecords($defaultRecords, $localizedRecords);
		return $childRecords;
	}

	protected function children(array $containerRecord, int $language): array
	{
		return $this->database->fetchRecordsByParentAndLanguage((int)$containerRecord['uid'], $language);
	}

	protected function sortLocalizedRecordsByDefaultRecords(array $defaultRecords, array $localizedRecords): array
	{
		$sorted = [];
		foreach ($defaultRecords as $defaultRecord)
		{
			foreach ($localizedRecords as $localizedRecord)
			{
				if ($localizedRecord['l18n_parent'] === $defaultRecord['uid'] || $localizedRecord['l18n_parent'] === $defaultRecord['t3ver_oid'])
				{
					$sorted[] = $localizedRecord;
				}
			}
		}
		return $sorted;
	}

	protected function recordsByColPosKey(array $records): array
	{
		$recordsByColPosKey = [];
		foreach ($records as $record)
		{
			if (empty($recordsByColPosKey[$record['colPos']]))
			{
				$recordsByColPosKey[$record['colPos']] = [];
			}
			$recordsByColPosKey[$record['colPos']][] = $record;
		}
		return $recordsByColPosKey;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}