<?php
namespace Maagit\Maagitcontainer\Domain\Factory;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Factory
	class:				Database

	description:		Database Factory

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Database implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var int
	 */
	protected $backendUserId = 0;

	/**
	 * @var int
	 */
	protected $workspaceId = 0;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\TYPO3\CMS\Core\Context\Context $context)
    {
		$this->backendUserId = (int)$context->getPropertyFromAspect('backend.user', 'id', 0);
		$this->workspaceId = (int)$context->getPropertyFromAspect('workspace', 'id');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function fetchRecordsByPidAndLanguage(int $pid, int $language): array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder->select('*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter($language, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->eq(
					'pid',
					$queryBuilder->createNamedParameter($pid, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->orderBy('sorting', 'ASC')
			->executeQuery();
		return (array)$stm->fetchAllAssociative();
	}

	public function fetchOneRecord(int $uid): ?array
	{
		$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('tt_content');
		$queryBuilder->getRestrictions()->removeAll();
		$stm = $queryBuilder->select('*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'uid',
					$queryBuilder->createNamedParameter($uid, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$record = $stm->fetchAssociative();
		if ($record === false)
		{
			return null;
		}
		return $record;
	}

	public function fetchOneDefaultRecord(array $record): ?array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder->select('*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'uid',
					$queryBuilder->createNamedParameter($record['l18n_parent'], \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$record = $stm->fetchAssociative();
		if ($record === false)
		{
			return null;
		}
		return $record;
	}

	public function fetchRecordsByParentAndLanguage(int $parent, int $language): array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder->select('*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'tx_maagitcontainer_parent',
					$queryBuilder->createNamedParameter($parent, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter($language, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->orderBy('sorting', 'ASC')
			->executeQuery();
		return (array)$stm->fetchAllAssociative();
	}

	public function fetchOverlayRecords(array $records, int $language): array
	{
		$uids = [];
		foreach ($records as $record)
		{
			$uids[] = $record['uid'];
			if ($record['t3ver_oid'] > 0)
			{
				$uids[] = $record['t3ver_oid'];
			}
		}
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder->select('*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'l18n_parent',
					$queryBuilder->createNamedParameter($uids, TYPO3\CMS\Core\Database\Connection::PARAM_INT_ARRAY)
				),
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter($language, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		return (array)$stm->fetchAllAssociative();
	}

	public function fetchOneOverlayRecord(int $uid, int $language): ?array
	{
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder->select('*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq(
					'l18n_parent',
					$queryBuilder->createNamedParameter($uid, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				),
				$queryBuilder->expr()->eq(
					'sys_language_uid',
					$queryBuilder->createNamedParameter($language, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$record = $stm->fetchAssociative();
		if ($record === false)
		{
			return null;
		}
		return $record;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function getQueryBuilder(): \TYPO3\CMS\Core\Database\Query\QueryBuilder
	{
		$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('tt_content');
		if ($this->getServerRequest() instanceof \Psr\Http\Message\ServerRequestInterface && \TYPO3\CMS\Core\Http\ApplicationType::fromRequest($this->getServerRequest())->isFrontend())
		{
			$queryBuilder->setRestrictions(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer::class));
			$queryBuilder->getRestrictions()
				->add(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\Query\Restriction\WorkspaceRestriction::class, $this->workspaceId));
		}
		else
		{
			$queryBuilder->getRestrictions()
				->removeAll()
				->add(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction::class))
				->add(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\Query\Restriction\WorkspaceRestriction::class, $this->workspaceId));
		}
		return $queryBuilder;
	}

	protected function getServerRequest(): ?\TYPO3\CMS\Core\Http\ServerRequest
	{
		return $GLOBALS['TYPO3_REQUEST'] ?? null;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}