<?php
namespace Maagit\Maagitcontainer\Domain\Factory\PageView;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Factory
	class:				ContentStorage

	description:		PageView Content storage

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


abstract class ContentStorage
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var ?mixed[]
	 */
	protected $records;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\Database
	 */
	protected $database;

	/**
	 * @var int
	 */
	protected $workspaceId = 0;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function __construct(\Maagit\Maagitcontainer\Domain\Factory\Database $database, \TYPO3\CMS\Core\Context\Context $context)
	{
		$this->database = $database;
		$this->workspaceId = (int)$context->getPropertyFromAspect('workspace', 'id');
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	abstract public function workspaceOverlay(array $records): array;

	abstract public function containerRecordWorkspaceOverlay(array $record): ?array;

	public function getContainerChildren(array $containerRecord, int $language): array
	{
		$pid = (int)$containerRecord['pid'];
        if (isset($containerRecord['t3ver_oid']) && $containerRecord['t3ver_oid'] > 0)
		{
			$defaultContainerRecord = $this->database->fetchOneRecord($containerRecord['t3ver_oid']);
			$uid = $defaultContainerRecord['uid'];
		}
		else
		{
			$uid = $containerRecord['uid'];
		}
		if (!isset($this->records[$pid][$language]))
		{
			$this->records[$pid][$language] = $this->buildRecords($pid, $language);
		}
		if (empty($this->records[$pid][$language][$uid]))
		{
			return [];
		}
		return $this->records[$pid][$language][$uid];
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function buildRecords(int $pid, int $language): array
	{
		$records = $this->database->fetchRecordsByPidAndLanguage($pid, $language);
		$records = $this->workspaceOverlay($records);
		$records = $this->recordsByContainer($records);
		return $records;
	}

	protected function recordsByContainer(array $records): array
	{
		$recordsByContainer = [];
		foreach ($records as $record)
		{
			if ($record['tx_maagitcontainer_parent'] > 0)
			{
				if (empty($recordsByContainer[$record['tx_maagitcontainer_parent']]))
				{
					$recordsByContainer[$record['tx_maagitcontainer_parent']] = [];
				}
				$recordsByContainer[$record['tx_maagitcontainer_parent']][] = $record;
			}
		}
		return  $recordsByContainer;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}