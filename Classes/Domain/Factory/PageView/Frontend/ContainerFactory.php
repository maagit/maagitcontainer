<?php
namespace Maagit\Maagitcontainer\Domain\Factory\PageView\Frontend;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Factory
	class:				ContainerFactory

	description:		Frontend Container Factory

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContainerFactory extends \Maagit\Maagitcontainer\Domain\Factory\PageView\ContainerFactory
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\PageView\Frontend\ContentStorage
	 */
	protected $contentStorage;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function __construct(\Maagit\Maagitcontainer\Domain\Factory\Database $database, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \TYPO3\CMS\Core\Context\Context $context, \Maagit\Maagitcontainer\Domain\Factory\PageView\Frontend\ContentStorage $contentStorage)
	{
		parent::__construct($database, $tcaRegistry, $context);
		$this->contentStorage = $contentStorage;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function buildContainer(int $uid): \Maagit\Maagitcontainer\Domain\Model\Container
	{
		/** @var LanguageAspect $languageAspect */
		$languageAspect =  \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Context\Context::class)->getAspect('language');
		$language = $languageAspect->getId();
		if ($language > 0 && $languageAspect->doOverlays())
		{
			return $this->buildContainerWithOverlay($uid, $languageAspect);
		}
		return parent::buildContainer($uid);
	}
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function doOverlay(array $defaultRecords, \TYPO3\CMS\Core\Context\LanguageAspect $languageAspect): array
	{
		$overlayed = [];
		$pageRepository = $this->contentStorage->getPageRepository();
		foreach ($defaultRecords as $defaultRecord)
		{
			$overlay = $pageRepository->getLanguageOverlay('tt_content', $defaultRecord, $languageAspect);
			if ($overlay !== null)
			{
				$overlayed[] = $overlay;
			}
		}
		return $overlayed;
	}

	protected function buildContainerWithOverlay(int $uid, \TYPO3\CMS\Core\Context\LanguageAspect $languageAspect): \Maagit\Maagitcontainer\Domain\Model\Container
	{
		$language = $languageAspect->get('id');
		$record = $this->database->fetchOneOverlayRecord($uid, $language);
		if ($record === null)
		{
			$record = $this->database->fetchOneRecord($uid);
		}

		if ($record === null)
		{
			throw new \Maagit\Maagitcontainer\Domain\Factory\Exception('cannot fetch record with uid ' . $uid, 1576572852);
		}
		if (!$this->tcaRegistry->isContainerElement($record['CType']))
		{
			throw new \Maagit\Maagitcontainer\Domain\Factory\Exception('not a container element with uid ' . $uid, 1576572853);
		}

		$defaultRecord = null;
		if ($record['sys_language_uid'] > 0)
		{
			$defaultRecord = $this->defaultContainer($record);
			if ($defaultRecord === null)
			{
				// free mode
				$childRecords = $this->children($record, $record['sys_language_uid']);
			}
			else
			{
				// connected mode
				$childRecords = $this->children($defaultRecord, 0);
				$childRecords = $this->doOverlay($childRecords, $languageAspect);
			}
		}
		else
		{
			// container record with sys_language_uid=0
			$childRecords = $this->children($record, 0);
			$childRecords = $this->doOverlay($childRecords, $languageAspect);
		}
		$childRecordByColPosKey = $this->recordsByColPosKey($childRecords);
		if ($defaultRecord === null)
		{
            $container = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Domain\Model\Container::class, $record, $childRecordByColPosKey, $language);
		}
		else
		{
			$container = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Domain\Model\Container::class, $defaultRecord, $childRecordByColPosKey, $language);
		}
		return $container;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}