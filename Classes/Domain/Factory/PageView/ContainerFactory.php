<?php
namespace Maagit\Maagitcontainer\Domain\Factory\PageView;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Factory
	class:				ContainerFactory

	description:		PageView Container Factory

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContainerFactory extends \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\PageView\ContentStorage
	 */
	protected $contentStorage;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function children(array $containerRecord, int $language): array
	{
		return $this->contentStorage->getContainerChildren($containerRecord, $language);
	}

	protected function localizedRecordsByDefaultRecords(array $defaultRecords, int $language): array
	{
		$childRecords = parent::localizedRecordsByDefaultRecords($defaultRecords, $language);
		return $this->contentStorage->workspaceOverlay($childRecords);
	}

	protected function containerByUid(int $uid): ?array
	{
		$record =  $this->database->fetchOneRecord($uid);
		if ($record === null)
		{
			return null;
		}
		return $this->contentStorage->containerRecordWorkspaceOverlay($record);
	}

	protected function defaultContainer(array $localizedContainer): ?array
	{
		if (isset($localizedContainer['_ORIG_uid']))
		{
			$localizedContainer = $this->database->fetchOneRecord($localizedContainer['uid']);
		}
		$defaultRecord = $this->database->fetchOneDefaultRecord($localizedContainer);
		if ($defaultRecord === null)
		{
			return null;
		}
		return $this->contentStorage->containerRecordWorkspaceOverlay($defaultRecord);
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}