<?php
namespace Maagit\Maagitcontainer\Domain\Factory\PageView\Backend;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Factory
	class:				ContentStorage

	description:		Content storage

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContentStorage extends \Maagit\Maagitcontainer\Domain\Factory\PageView\ContentStorage
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function workspaceOverlay(array $records): array
	{
		$filtered = [];
		foreach ($records as $row)
		{
            \TYPO3\CMS\Backend\Utility\BackendUtility::workspaceOL('tt_content', $row, $this->workspaceId, true);
			$versionState = \TYPO3\CMS\Core\Versioning\VersionState::tryFrom($row['t3ver_state'] ?? 0);
			if ($row && !($versionState === \TYPO3\CMS\Core\Versioning\VersionState::DELETE_PLACEHOLDER))
			{
				$filtered[] = $row;
			}
		}
		return $filtered;
	}
 
	public function containerRecordWorkspaceOverlay(array $record): ?array
	{
        \TYPO3\CMS\Backend\Utility\BackendUtility::workspaceOL('tt_content', $record, $this->workspaceId, false);
		$versionState = \TYPO3\CMS\Core\Versioning\VersionState::tryFrom($row['t3ver_state'] ?? 0);
		if ($record && !($versionState === \TYPO3\CMS\Core\Versioning\VersionState::DELETE_PLACEHOLDER))
		{
			return $record;
		}
		return null;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}