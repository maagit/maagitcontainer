<?php
namespace Maagit\Maagitcontainer\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Model
	class:				Container

	description:		Container model

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Container
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var array
	 */
	protected $containerRecord;

	/**
	 * @var array
	 */
	protected $childRecords;

	/**
	 * @var int
	 */
	protected $language = 0;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
	 * @param array $containerRecord
	 * @param array $childRecords
	 * @param int $language
	 */
	public function __construct(array $containerRecord, array $childRecords, $language = 0)
	{
		$this->containerRecord = $containerRecord;
		$this->childRecords = $childRecords;
		$this->language = $language;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
	 * @return int
	 */
	public function getUid(): int
	{
		return (int)$this->containerRecord['uid'];
	}

	public function getUidOfLiveWorkspace(): int
	{
		if (isset($this->containerRecord['t3ver_oid']) && $this->containerRecord['t3ver_oid'] > 0)
		{
			return (int)$this->containerRecord['t3ver_oid'];
		}
		return $this->getUid();
	}

	/**
	 * @return int
	 */
	public function getPid(): int
	{
		if (!empty($this->containerRecord['_ORIG_pid']))
		{
			return (int)$this->containerRecord['_ORIG_pid'];
		}
		return (int)$this->containerRecord['pid'];
	}

	/**
	 * @return int
	 */
	public function getLanguage(): int
	{
		return $this->language;
	}

	/**
	 * @return string
	 */
	public function getCType(): string
	{
		return $this->containerRecord['CType'];
	}

	/**
	 * @return array
	 */
	public function getContainerRecord(): array
	{
		return $this->containerRecord;
	}

	/**
	 * @return array
	 */
	public function getChildRecords(): array
	{
		$childRecords = [];
		foreach ($this->childRecords as $colPos => $records)
		{
			$childRecords = array_merge($childRecords, $records);
		}
		return $childRecords;
	}

	public function getChildRecordsUids(): array
	{
		$uids = [];
		$childRecords = $this->getChildRecords();
		foreach ($childRecords as $childRecord)
		{
			$uids[] = $childRecord['uid'];
		}
		return $uids;
	}

	/**
	 * @return array
	 */
	public function getChildrenColPos(): array
	{
		return array_keys($this->childRecords);
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * @return bool
	 */
	public function isConnectedMode(): bool
	{
		return (int)$this->containerRecord['sys_language_uid'] === 0;
	}

	/**
	 * @param int $colPos
	 * @return array
     */
	public function getChildrenByColPos(int $colPos): array
	{
		if (empty($this->childRecords[$colPos]))
		{
			return [];
		}
		return $this->childRecords[$colPos];
	}

	public function hasChildInColPos(int $colPos, int $childUid): bool
	{
		if (!isset($this->childRecords[$colPos]))
		{
			return false;
		}
		foreach ($this->childRecords[$colPos] as $childRecord)
		{
			if ($childRecord['uid'] === $childUid) {
				return true;
			}
		}
		return false;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}