<?php
namespace Maagit\Maagitcontainer\Domain\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Service
	class:				ContainerService

	description:		Container Service

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContainerService implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $tcaRegistry;

    /**
     * @var \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory
     */
    protected $containerFactory;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function __construct(\Maagit\Maagitcontainer\Tca\Registry $tcaRegistry, \Maagit\Maagitcontainer\Domain\Factory\ContainerFactory $containerFactory)
	{
		$this->tcaRegistry = $tcaRegistry;
		$this->containerFactory = $containerFactory;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function getNewContentElementAtTopTargetInColumn(\Maagit\Maagitcontainer\Domain\Model\Container $container, int $targetColPos): int
	{
		$target = -$container->getUid();
		$previousRecord = null;
		$allColumns = $this->tcaRegistry->getAllAvailableColumnsColPos($container->getCType());
		foreach ($allColumns as $colPos)
		{
			if ($colPos === $targetColPos && $previousRecord !== null)
			{
				$target = -$previousRecord['uid'];
			}
			$children = $container->getChildrenByColPos($colPos);
			if (!empty($children))
			{
				$last = array_pop($children);
				$previousRecord = $last;
			}
		}
		return $target;
	}

    public function getAfterContainerElementTarget(\Maagit\Maagitcontainer\Domain\Model\Container $container): int
	{
		$target = -$container->getUid();
		$childRecords = $container->getChildRecords();
		if (empty($childRecords))
		{
			return $target;
		}
		$lastChild = array_pop($childRecords);
		if (!$this->tcaRegistry->isContainerElement($lastChild['CType']))
		{
			return -$lastChild['uid'];
		}
		$container = $this->containerFactory->buildContainer($lastChild['uid']);
		return $this->getAfterContainerElementTarget($container);
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}