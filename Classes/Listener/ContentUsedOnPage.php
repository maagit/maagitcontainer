<?php
namespace Maagit\Maagitcontainer\Listener;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Listener
	class:				ContentUsedOnPage

	description:		...

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContentUsedOnPage
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var \Maagit\Maagitcontainer\Tca\Registry
     */
    protected $tcaRegistry;

	/**
	 * @var \Maagit\Maagitcontainer\Domain\Factory\PageView\Backend\ContainerFactory
	 */
	protected $containerFactory;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Domain\Factory\PageView\Backend\ContainerFactory $containerFactory, \Maagit\Maagitcontainer\Tca\Registry $tcaRegistry)
	{
		$this->containerFactory = $containerFactory;
		$this->tcaRegistry = $tcaRegistry;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function __invoke(\TYPO3\CMS\Backend\View\Event\IsContentUsedOnPageLayoutEvent $event): void
	{
		$record = $event->getRecord();
		if ($record['tx_maagitcontainer_parent'] > 0)
		{
			try
			{
				$container = $this->containerFactory->buildContainer((int)$record['tx_maagitcontainer_parent']);
				$columns = $this->tcaRegistry->getAvailableColumns($container->getCType());
				foreach ($columns as $column)
				{
					if ($column['colPos'] === (int)$record['colPos'])
					{
						if ($record['sys_language_uid'] > 0 && $container->isConnectedMode())
						{
							$used = $container->hasChildInColPos($record['colPos'], $record['l18n_parent']);
							$event->setUsed($used);
							return;
						}
						$used = $container->hasChildInColPos($record['colPos'], $record['uid']);
						$event->setUsed($used);
						return;
					}
				}
				$event->setUsed(false);
				return;
			}
			catch (\Maagit\Maagitcontainer\Domain\Factory\Exception $e)
			{
				
			}
		}
	}
 
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}