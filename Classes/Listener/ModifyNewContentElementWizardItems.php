<?php
namespace Maagit\Maagitcontainer\Listener;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Listener
	class:				ModifyNewContentElementWizardItems

	description:		Modify New Content Element Wizard Items

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


#[\TYPO3\CMS\Core\Attribute\AsEventListenerAsEventListener(
    identifier: 'maagitcontainer/backend/modify-wizard-items',
)]
class ModifyNewContentElementWizardItems
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function __invoke(\TYPO3\CMS\Backend\Controller\Event\ModifyNewContentElementWizardItemsEvent $event): void
	{
		$parent = $this->getParentIdFromRequest();
		if ($parent !== null)
		{
			$wizardItems = $event->getWizardItems();
			foreach ($wizardItems as $key => $item)
			{
				if (strpos($key, '_') !== false)
				{
					$wizardItems[$key]['defaultValues']['tx_maagitcontainer_parent'] = $parent;
				}
			}
			$event->setWizardItems($wizardItems);
		}
	}
 
 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function getParentIdFromRequest(): ?int
	{
		$request = $this->getServerRequest();
		if ($request === null)
		{
			return null;
		}
		$queryParams = $request->getQueryParams();
		if (isset($queryParams['tx_maagitcontainer_parent']) && (int)$queryParams['tx_maagitcontainer_parent'] > 0)
		{
			return (int)$queryParams['tx_maagitcontainer_parent'];
		}
		return null;
	}

	protected function getServerRequest(): ?\TYPO3\CMS\Core\Http\ServerRequest
    {
		return $GLOBALS['TYPO3_REQUEST'] ?? null;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}