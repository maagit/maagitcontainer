<?php
namespace Maagit\Maagitcontainer\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2023-2023 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitcontainer
	Package:			Service
	class:				RecordLocalizeSummaryModifier

	description:		...

	created:			2023-02-21
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2023-02-21	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class RecordLocalizeSummaryModifier implements \TYPO3\CMS\Core\SingletonInterface
{
	/* ======================================================================================= */
	/* U S E   T R A I T S                                                                     */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
	 * @var \Maagit\Maagitcontainer\Tca\Registry
	 */
	protected $containerRegistry;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitcontainer\Tca\Registry $containerRegistry)
	{
		$this->containerRegistry = $containerRegistry;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	

	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function rebuildPayload(array $payload): array
	{
		return [
			'records' => $this->filterRecords($payload['records']),
			'columns' => $this->rebuildColumns($payload['columns']),
		];
	}
 
	public function filterRecords(array $recordsPerColPos): array
	{
		// cannot be done by event in v10
		$uids = [];
		foreach ($recordsPerColPos as $colPos => $records)
		{
			foreach ($records as $record)
			{
				$uids[] = $record['uid'];
			}
		}
		if (empty($uids))
		{
			return $recordsPerColPos;
		}
		$containerUids = $this->getContainerUids($uids);
		if (empty($containerUids))
		{
			return $recordsPerColPos;
		}
		$containerChildren = $this->getContainerChildren($uids);
		if (empty($containerChildren))
		{
			return $recordsPerColPos;
		}
		// we have both: container to translate and container children to translate
		// unset all records in container to translate
		$filtered = [];
		foreach ($recordsPerColPos as $colPos => $records)
		{
			$filteredRecords = [];
			foreach ($records as $record)
			{
				if (empty($containerChildren[$record['uid']]))
				{
					$filteredRecords[] = $record;
				}
				else
				{
					$fullRecord = $containerChildren[$record['uid']];
					if (!in_array($fullRecord['tx_maagitcontainer_parent'], $containerUids, true))
					{
						$filteredRecords[] = $record;
					}
				}
			}
			if (!empty($filteredRecords))
			{
				$filtered[$colPos] = $filteredRecords;
			}
		}
		return $filtered;
	}

	public function rebuildColumns(array $columns): array
	{
		// this can be done with AfterPageColumnsSelectedForLocalizationEvent event in v10
		$containerColumns = $this->containerRegistry->getAllAvailableColumns();
		foreach ($containerColumns as $containerColumn)
		{
			$columns = [
				'columns' => array_replace([$containerColumn['colPos'] => 'Container Children (' . $containerColumn['colPos'] . ')'], $columns['columns']),
				'columnList' => array_values(array_unique(array_merge([$containerColumn['colPos']], $columns['columnList']))),
			];
		}
		return $columns;
	}

 
	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	protected function getContainerUids(array $uids): array
	{
		$containerCTypes = $this->containerRegistry->getRegisteredCTypes();
		if (empty($containerCTypes))
		{
			return [];
		}
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder->select('uid', 'l18n_parent')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'uid',
					$queryBuilder->createNamedParameter($uids, \TYPO3\CMS\Core\Database\Connection::PARAM_INT_ARRAY)
				),
				$queryBuilder->expr()->in(
					'CType',
					$queryBuilder->createNamedParameter($containerCTypes, \TYPO3\CMS\Core\Database\Connection::PARAM_STR_ARRAY)
				)
			)
			->executeQuery();
		$rows = $stm->fetchAllAssociative();
		$containerUids = [];
		foreach ($rows as $row)
		{
			$containerUids[] = $row['uid'];
			if ($row['l18n_parent'] > 0)
			{
				$containerUids[] = $row['l18n_parent'];
			}
		}
		return $containerUids;
	}

	protected function getContainerChildren(array $uids): array
	{
		$containerChildren = [];
		$queryBuilder = $this->getQueryBuilder();
		$stm = $queryBuilder->select('*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->in(
					'uid',
					$queryBuilder->createNamedParameter($uids, \TYPO3\CMS\Core\Database\Connection::PARAM_INT_ARRAY)
				),
				$queryBuilder->expr()->neq(
					'tx_maagitcontainer_parent',
					$queryBuilder->createNamedParameter(0, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->executeQuery();
		$rows = $stm->fetchAllAssociative();
		foreach ($rows as $row)
		{
			$containerChildren[$row['uid']] = $row;
		}
		return $containerChildren;
	}

	protected function getQueryBuilder(): QueryBuilder
	{
		$queryBuilder = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getQueryBuilderForTable('tt_content');
		$queryBuilder->getRestrictions()
			->removeAll()
			->add(\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction::class));
		return $queryBuilder;
	}

		
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}