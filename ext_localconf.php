<?php
defined('TYPO3') or die();

call_user_func(static function () {
    $commandMapHooks = [
        'tx_maagitcontainer-post-process' => \Maagit\Maagitcontainer\Hooks\Datahandler\CommandMapPostProcessingHook::class,
        'tx_maagitcontainer-before-start' => \Maagit\Maagitcontainer\Hooks\Datahandler\CommandMapBeforeStartHook::class,
        'tx_maagitcontainer-delete' => \Maagit\Maagitcontainer\Hooks\Datahandler\DeleteHook::class,
        'tx_maagitcontainer-after-finish' => \Maagit\Maagitcontainer\Hooks\Datahandler\CommandMapAfterFinishHook::class,
    ];

    $datamapHooks = [
        'tx_maagitcontainer-before-start' => \Maagit\Maagitcontainer\Hooks\Datahandler\DatamapBeforeStartHook::class,
        'tx_maagitcontainer-pre-process-field-array' => \Maagit\Maagitcontainer\Hooks\Datahandler\DatamapPreProcessFieldArrayHook::class,
    ];

    // set our hooks at the beginning of Datamap Hooks
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'] = array_merge(
        $commandMapHooks,
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'] ?? []
    );
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'] = array_merge(
        $datamapHooks,
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']
    );

	// XCLASS for setting "tx_maagitcontainer_parent" on creating new records in given container
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\FormDataProvider\DatabaseRowInitializeNew::class] = [
		'className' => \Maagit\Maagitcontainer\Xclasses\DatabaseRowInitializeNew::class,
	];
});
