<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MaagIT Container',
	'description' => 'Content container functionality for using websites provided by TYPO3 CMS.',
	'category' => 'plugin',
	'author' => 'Urs Maag',
	'author_email' => 'info@maagit.ch',
	'author_company' => 'maagIT',
	'state' => 'stable',
	'createDirs' => '',
	'clearCacheOnLoad' => 1,
	'version' => '13.4.8',
	'constraints' => [
		'depends' => [
			'typo3' => '13.4.8-13.99.99'
		],
		'conflicts' => [

		],
		'suggests' => [

		]
	]
];
