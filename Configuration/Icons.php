<?php

return [
    '1col' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:maagitcontainer/Resources/Public/Icons/1col.svg',
    ],
    '2col' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:maagitcontainer/Resources/Public/Icons/2col.svg',
    ],
    '3col' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:maagitcontainer/Resources/Public/Icons/3col.svg',
    ],
    '4col' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:maagitcontainer/Resources/Public/Icons/4col.svg',
    ],
];
