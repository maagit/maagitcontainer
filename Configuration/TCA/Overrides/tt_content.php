<?php
defined('TYPO3') || die('Access denied.');

call_user_func(static function () {
	$additionalColumns = [
        'tx_maagitcontainer_parent' => [
            'label' => 'LLL:EXT:maagitcontainer/Resources/Private/Language/locallang.xlf:container',
            'config' => [
                'default' => 0,
                'type' => 'select',
                'foreign_table' => 'tt_content',
                // if not set, all content elements are already loaded before itemsProcFunc is called
                'foreign_table_where' => ' AND 1=2',
                'itemsProcFunc' => \Maagit\Maagitcontainer\Tca\ItemProcFunc::class . '->txMaagitcontainerParent',
                'renderType' => 'selectSingle',
            ],
        ],
        'tx_maagitcontainer_samecolumnlength' => [
            'label' => 'LLL:EXT:maagitcontainer/Resources/Private/Language/locallang.xlf:samecolumnlength',
			'config' => [
				'type' => 'check',
				'items' => [
					[
						'label' => 'LLL:EXT:maagitcontainer/Resources/Private/Language/locallang.xml:samecolumnlength',
						'labelChecked' => 'Enabled',
						'labelUnchecked' => 'Disabled'
					]
				]
			]
		]
	];

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $additionalColumns
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'general',
        'tx_maagitcontainer_parent'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'general',
        '--linebreak--'
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'tt_content',
        'general',
        'tx_maagitcontainer_samecolumnlength'
    );

    $GLOBALS['TCA']['tt_content']['columns']['CType']['config']['itemGroups']['container'] = 'LLL:EXT:maagitcontainer/Resources/Private/Language/locallang.xlf:container';

    $GLOBALS['TCA']['tt_content']['columns']['colPos']['config']['itemsProcFunc'] = \Maagit\Maagitcontainer\Tca\ItemProcFunc::class . '->colPos';

    // copyAfterDuplFields colPos,sys_language_uid
    // useColumnsForDefaultValues colPos,sys_language_uid,CType
    // new element
    $GLOBALS['TCA']['tt_content']['ctrl']['useColumnsForDefaultValues'] .= ',tx_maagitcontainer_parent,tx_maagitcontainer_samecolumnlength';


	// Default 2 column container
	\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Tca\Registry::class)->configureContainer(
		(
			new \Maagit\Maagitcontainer\Tca\ContainerConfiguration(
				'2colContainer', 
				'2 Spalten',
				'Zwei Spalten',
				[
					[
						['name' => 'Linke Spalte', 'colPos' => 201],
						['name' => 'Rechte Spalte', 'colPos' => 202]
					]
				]
			)
		) 
	);

	// Default 3 column container
	\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Tca\Registry::class)->configureContainer(
		(
			new \Maagit\Maagitcontainer\Tca\ContainerConfiguration(
				'3colContainer', 
				'3 Spalten',
				'Drei Spalten',
				[
					[
						['name' => 'Linke Spalte', 'colPos' => 301],
						['name' => 'Mittlere Spalte', 'colPos' => 302],
						['name' => 'Rechte Spalte', 'colPos' => 303]
					]
				]
			)
		) 
	);

	// Default 4 column container
	\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Maagit\Maagitcontainer\Tca\Registry::class)->configureContainer(
		(
			new \Maagit\Maagitcontainer\Tca\ContainerConfiguration(
				'4colContainer', 
				'4 Spalten',
				'Vier Spalten',
				[
					[
						['name' => 'Linke Spalte', 'colPos' => 401],
						['name' => 'Zweite Spalte', 'colPos' => 402],
						['name' => 'Dritte Spalte', 'colPos' => 403],
						['name' => 'Rechte Spalte', 'colPos' => 404]
					]
				]
			)
		) 
	);
});
